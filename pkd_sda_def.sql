-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2020 at 08:21 PM
-- Server version: 10.1.45-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krakatio_pkd`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_allDesa` ()  BEGIN
 SELECT int_kecamatan_id,int_desa_id,txt_desa FROM m_desa ORDER BY txt_desa ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_allPembayaran_retribusi` (IN `in_int_userid_petugas` INT, IN `in_int_progress` INT, IN `in_int_kecamatan_id` INT, IN `in_int_desa_id` BIGINT)  BEGIN

DECLARE where_progress VARCHAR(300);
DECLARE where_kecamatan VARCHAR(300);
DECLARE where_desa VARCHAR(300);

IF in_int_progress = -1 THEN 
	SET where_progress = CONCAT(" AND (PR.int_pemeriksaan_id NOT IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL) OR PR.int_pemeriksaan_id IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL))");
ELSE
	IF in_int_progress = 1 THEN
		SET where_progress = CONCAT(" AND PR.int_pemeriksaan_id IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL)");
	ELSE
		SET where_progress = CONCAT(" AND PR.int_pemeriksaan_id NOT IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL)");
	END IF;
END IF;

IF (in_int_kecamatan_id = -1 OR in_int_kecamatan_id IS NULL OR in_int_kecamatan_id = '') THEN 
	 	SET where_kecamatan =CONCAT(" AND PM.int_kecamatan_id_permohonan IN (SELECT A.int_kecamatan_id FROM m_userpetugas_akses A WHERE A.int_userid_petugas = ",in_int_userid_petugas,")");
ELSE
		SET where_kecamatan = CONCAT(" AND PM.int_kecamatan_id_permohonan=",in_int_kecamatan_id);
END IF;

IF (in_int_desa_id = -1 OR in_int_desa_id IS NULL OR in_int_desa_id = '') THEN 
	 SET where_desa = "";
ELSE
	 SET where_desa = CONCAT(" AND PM.int_desa_id_permohonan=",in_int_desa_id);

END IF;

SET @query =CONCAT("SELECT PR.int_pemeriksaan_id, PR.int_permohonan_id, PM.txt_nama_pemohon, PM.txt_keperluan_permohonan, PM.txt_letak_tanah_permohonan, PR.num_panjang_pemeriksaan, PR.num_lebar_pemeriksaan, PR.num_luas_pemeriksaan, (SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = PM.int_kecamatan_id_permohonan LIMIT 1) AS txt_kecamatan, (SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = PM.int_kecamatan_id_permohonan AND D.int_desa_id = PM.int_desa_id_permohonan LIMIT 1) AS txt_desa, PM.txt_koordinat_pemohon, PR.txt_no_skr, PM.dt_tgl_awal, PM.dt_tgl_akhir, PR.dt_jatuh_tempo, PR.txt_fungsi, (SELECT TF.cur_tarif_fungsi FROM m_tarif_fungsi TF WHERE TF.int_tarif_fungsi_id = PR.int_tarif_fungsi_id) AS cur_tarif_fungsi, IFNULL(PR.num_retribusi,0) AS num_retribusi, PM.num_panjang_permohonan, PM.num_lebar_permohonan, PM.num_luas_permohonan, PM.dt_created, PR.dt_pemeriksaan_petugas, (SELECT PL.dt_pelunasan FROM t_pelunasan PL WHERE PL.int_pemeriksaan_id = PR.int_pemeriksaan_id) as dt_pelunasan, (SELECT COUNT(int_pemeriksaan_id) FROM t_pelunasan PL WHERE PL.int_pemeriksaan_id = PR.int_pemeriksaan_id) as int_progress FROM t_pemeriksaan PR JOIN t_permohonan PM ON PM.int_permohonan_id = PR.int_permohonan_id WHERE 1=1",where_progress,where_kecamatan,where_desa, " AND PR.int_valid = 2");
 PREPARE stmt FROM @query;
 EXECUTE stmt;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_allPemeriksaan_asset` (IN `in_int_userid_petugas` INT, IN `in_int_progress` INT, IN `in_int_kecamatan_id` INT, IN `in_int_desa_id` BIGINT)  BEGIN

DECLARE where_progress VARCHAR(300);
DECLARE where_kecamatan VARCHAR(300);
DECLARE where_desa VARCHAR(300);

IF in_int_progress = -1 THEN 
	SET where_progress = CONCAT(" AND (PR.int_valid = 0 OR PR.int_valid = 3)");
ELSE
	SET where_progress = CONCAT(" AND (PR.int_valid = 0 OR PR.int_valid = 3)");
END IF;

IF (in_int_kecamatan_id = -1 OR in_int_kecamatan_id IS NULL OR in_int_kecamatan_id = '') THEN 
	 	SET where_kecamatan =CONCAT(" AND PM.int_kecamatan_id_permohonan IN (SELECT A.int_kecamatan_id FROM m_userpetugas_akses A WHERE A.int_userid_petugas = ",in_int_userid_petugas,")");
ELSE
		SET where_kecamatan = CONCAT(" AND PM.int_kecamatan_id_permohonan=",in_int_kecamatan_id);
END IF;

IF (in_int_desa_id = -1 OR in_int_desa_id IS NULL OR in_int_desa_id = '') THEN 
	 SET where_desa = "";
ELSE
	 SET where_desa = CONCAT(" AND PM.int_desa_id_permohonan=",in_int_desa_id);

END IF;

SET @query =CONCAT("SELECT PR.int_pemeriksaan_id, PR.int_permohonan_id, PM.txt_nama_pemohon, PM.txt_keperluan_permohonan, PM.txt_letak_tanah_permohonan, PR.num_panjang_pemeriksaan, PR.num_lebar_pemeriksaan, PR.num_luas_pemeriksaan, (SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = PM.int_kecamatan_id_permohonan) AS txt_kecamatan, (SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = PM.int_kecamatan_id_permohonan AND D.int_desa_id = PM.int_desa_id_permohonan) AS txt_desa, PR.int_jaringan_id, (SELECT J.txt_jaringan FROM m_jaringan J WHERE J.int_jaringan_id = PR.int_jaringan_id) AS txt_jaringan, (SELECT J.txt_kode FROM m_jaringan J WHERE J.int_jaringan_id = PR.int_jaringan_id) AS txt_kode_jaringan, PR.txt_fungsi, PR.txt_latitude, PR.txt_longitude, PM.dt_created, PR.dt_pemeriksaan_petugas, PM.num_panjang_permohonan, PM.num_lebar_permohonan, PM.num_luas_permohonan, PR.int_valid as int_progress FROM t_pemeriksaan PR JOIN t_permohonan PM ON PR.int_permohonan_id = PM.int_permohonan_id WHERE 1=1",where_progress,where_kecamatan,where_desa);
 PREPARE stmt FROM @query;
 EXECUTE stmt;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_dashboard` (IN `in_int_userid_petugas` INT)  BEGIN

DECLARE asset_belum_diperiksa INT;
DECLARE asset_diperiksa 			INT;

DECLARE retribusi_belum_dibayar	INT;
DECLARE cur_retribusi_belum_dibayar DOUBLE(11,0);
DECLARE retribusi_dibayar	INT;
DECLARE cur_retribusi_dibayar	DOUBLE(11,0);

DECLARE cur_belum_disetor	DOUBLE(11,0);
DECLARE cur_sudah_disetor	DOUBLE(11,0);

SET asset_belum_diperiksa = (SELECT count(int_pemeriksaan_id) AS asset_belum_diperiksa FROM t_pemeriksaan WHERE (int_valid = 0 OR int_valid = 3) AND int_permohonan_id IN (SELECT int_permohonan_id FROM t_permohonan WHERE int_kecamatan_id_permohonan IN (SELECT int_kecamatan_id FROM m_userpetugas_akses WHERE int_userid_petugas = in_int_userid_petugas)));

SET asset_diperiksa = (SELECT count(int_pemeriksaan_id) AS asset_diperiksa FROM t_pemeriksaan WHERE int_userid_petugas = in_int_userid_petugas AND (int_valid = 1 OR int_valid = 2) AND dt_pemeriksaan_petugas BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW());

SET retribusi_belum_dibayar = (SELECT count(int_pemeriksaan_id) AS retribusi_belum_dibayar FROM t_pemeriksaan WHERE int_permohonan_id IN (SELECT int_permohonan_id FROM t_permohonan WHERE int_kecamatan_id_permohonan IN (SELECT int_kecamatan_id FROM m_userpetugas_akses WHERE int_userid_petugas = in_int_userid_petugas)) AND int_pemeriksaan_id NOT IN (SELECT int_pemeriksaan_id from t_pelunasan));

SET cur_retribusi_belum_dibayar = (SELECT IFNULL(SUM(num_retribusi), 0) AS cur_retribusi_belum_dibayar FROM t_pemeriksaan WHERE int_permohonan_id IN (SELECT int_permohonan_id FROM t_permohonan WHERE int_kecamatan_id_permohonan IN (SELECT int_kecamatan_id FROM m_userpetugas_akses WHERE int_userid_petugas = in_int_userid_petugas)) AND int_pemeriksaan_id NOT IN (SELECT int_pemeriksaan_id from t_pelunasan));

SET retribusi_dibayar = (SELECT count(int_pelunasan_id) AS retribusi_dibayar FROM t_pelunasan WHERE int_petugas_id = in_int_userid_petugas AND dt_pelunasan BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW());

SET cur_retribusi_dibayar = (SELECT IFNULL(SUM(num_retribusi), 0) AS cur_retribusi_dibayar FROM t_pemeriksaan WHERE int_pemeriksaan_id IN (SELECT int_pemeriksaan_id FROM t_pelunasan WHERE int_petugas_id = in_int_userid_petugas AND dt_pelunasan BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()));


SET cur_belum_disetor = 0;

SET cur_sudah_disetor = 0;

select asset_belum_diperiksa,
			asset_diperiksa,
			retribusi_belum_dibayar,
			cur_retribusi_belum_dibayar,
			retribusi_dibayar,
			cur_retribusi_dibayar,
			cur_belum_disetor,
			cur_sudah_disetor;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_datapemohon_pembayaran` (IN `in_int_permohonan_id` INT, IN `in_int_pemeriksaan_id` INT)  BEGIN
SELECT 
	P.int_permohonan_id,
	P.dt_created,
	P.txt_nama_pemohon,
	(SELECT PP.txt_no_skr from t_pemeriksaan PP WHERE PP.int_pemeriksaan_id = in_int_pemeriksaan_id LIMIT 1) as txt_no_skr,
	P.dt_tgl_awal,
	P.dt_tgl_akhir,
	P.txt_letak_tanah_permohonan,
	(SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = P.int_kecamatan_id_permohonan LIMIT 1) AS txt_kecamatan,
	(SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = P.int_kecamatan_id_permohonan AND D.int_desa_id = P.int_desa_id_permohonan LIMIT 1) AS txt_desa, 
	P.txt_koordinat_pemohon,
	(SELECT PP.dt_jatuh_tempo from t_pemeriksaan PP WHERE PP.int_pemeriksaan_id = in_int_pemeriksaan_id LIMIT 1) as dt_jatuh_tempo,
	(SELECT PP.txt_tarif_fungsi from m_tarif_fungsi PP WHERE PP.int_tarif_fungsi_id = 
	(SELECT TT.int_tarif_fungsi_id from t_pemeriksaan TT WHERE TT.int_pemeriksaan_id = in_int_pemeriksaan_id LIMIT 1)) as txt_fungsi,
	(SELECT PP.cur_tarif_fungsi from m_tarif_fungsi PP WHERE PP.int_tarif_fungsi_id = 
	(SELECT TT.int_tarif_fungsi_id from t_pemeriksaan TT WHERE TT.int_pemeriksaan_id = in_int_pemeriksaan_id LIMIT 1)) as cur_tarif_fungsi,
	P.num_panjang_permohonan,
	P.num_lebar_permohonan,
	P.num_luas_permohonan,
	(SELECT PP.num_retribusi from t_pemeriksaan PP WHERE PP.int_pemeriksaan_id = in_int_pemeriksaan_id LIMIT 1) as num_retribusi
FROM t_permohonan P WHERE P.int_permohonan_id = in_int_permohonan_id;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_datapemohon_pemeriksaan` (IN `in_int_permohonan_id` INT, IN `in_int_pemeriksaan_id` INT)  BEGIN
SELECT 
	P.int_permohonan_id,
	(SELECT PR.dt_pemeriksaan_petugas from t_pemeriksaan PR WHERE PR.int_pemeriksaan_id = in_int_pemeriksaan_id) as dt_created,
	P.txt_nama_pemohon,
	P.txt_letak_tanah_permohonan,
	(SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = P.int_kecamatan_id_permohonan LIMIT 1) AS txt_kecamatan,
	(SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = P.int_kecamatan_id_permohonan AND D.int_desa_id = P.int_desa_id_permohonan LIMIT 1) AS txt_desa, 
	P.num_panjang_permohonan,
	P.num_lebar_permohonan,
	P.num_luas_permohonan,
	P.txt_keperluan_permohonan,
	P.txt_letak_tanah_permohonan
FROM t_permohonan P WHERE P.int_permohonan_id = in_int_permohonan_id;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_desa` (IN `in_int_kecamatan_id` INT)  BEGIN
 SELECT int_kecamatan_id,int_desa_id,txt_desa FROM m_desa WHERE int_kecamatan_id = in_int_kecamatan_id ORDER BY txt_desa ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_jaringanirigasi` (IN `in_int_userid_petugas` INT)  BEGIN
 SELECT int_jaringan_id,txt_jaringan FROM m_jaringan ORDER BY txt_jaringan ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_kecamatan` (IN `in_int_userid_petugas` INT)  BEGIN
 SELECT int_kecamatan_id,txt_kecamatan FROM m_kecamatan WHERE int_kecamatan_id IN (SELECT int_kecamatan_id from m_userpetugas_akses WHERE int_userid_petugas = in_int_userid_petugas)  ORDER BY txt_kecamatan ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_pembayaran_retribusi` (IN `in_int_userid_petugas` INT, IN `in_int_progress` INT, IN `in_int_kecamatan_id` INT, IN `in_int_desa_id` BIGINT, IN `in_dtawal` DATE, IN `in_dtakhir` DATE)  BEGIN

DECLARE where_progress VARCHAR(300);
DECLARE where_kecamatan VARCHAR(300);
DECLARE where_desa VARCHAR(300);

IF in_int_progress = -1 THEN 
	SET where_progress = CONCAT(" AND (PR.int_pemeriksaan_id NOT IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL) OR PR.int_pemeriksaan_id IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL))");
ELSE
	IF in_int_progress = 1 THEN
		SET where_progress = CONCAT(" AND PR.int_pemeriksaan_id IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL)");
	ELSE
		SET where_progress = CONCAT(" AND PR.int_pemeriksaan_id NOT IN (SELECT PL.int_pemeriksaan_id FROM t_pelunasan PL)");
	END IF;
END IF;

IF (in_int_kecamatan_id = -1 OR in_int_kecamatan_id IS NULL OR in_int_kecamatan_id = '') THEN 
	 	SET where_kecamatan =CONCAT(" AND PM.int_kecamatan_id_permohonan IN (SELECT A.int_kecamatan_id FROM m_userpetugas_akses A WHERE A.int_userid_petugas = ",in_int_userid_petugas,")");
ELSE
		SET where_kecamatan = CONCAT(" AND PM.int_kecamatan_id_permohonan=",in_int_kecamatan_id);
END IF;

IF (in_int_desa_id = -1 OR in_int_desa_id IS NULL OR in_int_desa_id = '') THEN 
	 SET where_desa = "";
ELSE
	 SET where_desa = CONCAT(" AND PM.int_desa_id_permohonan=",in_int_desa_id);

END IF;

SET @query =CONCAT("SELECT PR.int_pemeriksaan_id,
PR.int_permohonan_id,
PR.dt_pemeriksaan_petugas as dt_created,
PM.txt_nama_pemohon,
PM.txt_keperluan_permohonan,
PM.txt_letak_tanah_permohonan,
(SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = PM.int_kecamatan_id_permohonan LIMIT 1) AS txt_kecamatan,
(SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = PM.int_kecamatan_id_permohonan AND D.int_desa_id = PM.int_desa_id_permohonan LIMIT 1) AS txt_desa,
PM.num_luas_permohonan,
IFNULL(PR.num_retribusi,0) AS num_retribusi,
(SELECT COUNT(int_pemeriksaan_id) FROM t_pelunasan PL WHERE PL.int_pemeriksaan_id = PR.int_pemeriksaan_id) as int_progress
FROM t_pemeriksaan PR
JOIN t_permohonan PM ON PM.int_permohonan_id = PR.int_permohonan_id
WHERE 1=1",where_progress,where_kecamatan,where_desa, " AND DATE(PR.dt_pemeriksaan_petugas) >= '",in_dtawal,"' AND DATE(PR.dt_pemeriksaan_petugas) <= '",in_dtakhir,"'", " AND PR.int_valid = 2");
 PREPARE stmt FROM @query;
 EXECUTE stmt;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_pemeriksaan_asset` (IN `in_int_userid_petugas` INT, IN `in_int_progress` INT, IN `in_int_kecamatan_id` INT, IN `in_int_desa_id` BIGINT, IN `in_dtawal` DATE, IN `in_dtakhir` DATE)  BEGIN

DECLARE where_progress VARCHAR(300);
DECLARE where_kecamatan VARCHAR(300);
DECLARE where_desa VARCHAR(300);

IF in_int_progress = -1 THEN 
	SET where_progress = CONCAT(" AND (PR.int_valid = 0 OR PR.int_valid = 3)");
ELSE
	SET where_progress = CONCAT(" AND (PR.int_valid = 0 OR PR.int_valid = 3)");
END IF;

IF (in_int_kecamatan_id = -1 OR in_int_kecamatan_id IS NULL OR in_int_kecamatan_id = '') THEN 
	 	SET where_kecamatan =CONCAT(" AND PM.int_kecamatan_id_permohonan IN (SELECT A.int_kecamatan_id FROM m_userpetugas_akses A WHERE A.int_userid_petugas = ",in_int_userid_petugas,")");
ELSE
		SET where_kecamatan = CONCAT(" AND PM.int_kecamatan_id_permohonan=",in_int_kecamatan_id);
END IF;

IF (in_int_desa_id = -1 OR in_int_desa_id IS NULL OR in_int_desa_id = '') THEN 
	 SET where_desa = "";
ELSE
	 SET where_desa = CONCAT(" AND PM.int_desa_id_permohonan=",in_int_desa_id);

END IF;

SET @query =CONCAT("SELECT PR.int_pemeriksaan_id,
PR.int_permohonan_id,
PR.dt_pemeriksaan_petugas,
PM.txt_nama_pemohon,
PM.txt_keperluan_permohonan,
PM.txt_letak_tanah_permohonan,
(SELECT K.txt_kecamatan FROM m_kecamatan K WHERE K.int_kecamatan_id = PM.int_kecamatan_id_permohonan) AS txt_kecamatan,
(SELECT D.txt_desa FROM m_desa D WHERE D.int_kecamatan_id = PM.int_kecamatan_id_permohonan AND D.int_desa_id = PM.int_desa_id_permohonan) AS txt_desa,
PM.num_luas_permohonan,
PR.int_valid as int_progress
FROM t_pemeriksaan PR
JOIN t_permohonan PM ON PR.int_permohonan_id = PM.int_permohonan_id WHERE 1=1",where_progress,where_kecamatan,where_desa, " AND DATE(PR.dt_pemeriksaan_petugas) >= '",in_dtawal,"' AND DATE(PR.dt_pemeriksaan_petugas) <= '",in_dtakhir,"'");
 PREPARE stmt FROM @query;
 EXECUTE stmt;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_get_userAccount` (IN `in_txtusername` VARCHAR(60))  NO SQL
BEGIN
 IF EXISTS(SELECT int_userid_petugas FROM m_userpetugas WHERE (txt_username = in_txtusername OR txt_email = in_txtusername) AND is_aktif = 1)
   THEN
     SELECT 1 AS success, int_userid_petugas, txt_username, txt_password, txt_email, txt_namadepan, txt_avatar FROM m_userpetugas WHERE (txt_username = in_txtusername OR txt_email = in_txtusername) AND is_aktif = 1;
   ELSE
      SELECT 0 AS success, 0 AS int_userid_petugas, '' AS txt_username, '' AS txt_email, '' AS txt_namadepan, '' AS txt_avatar;
   END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_ins_datapembayaran` (IN `in_int_pemeriksaan_id` INT, IN `in_int_permohonan_id` INT, IN `in_txt_keterangan` TEXT, IN `in_int_userid_petugas` INT, IN `in_dt_pemeriksaan_petugas` DATE)  BEGIN

DECLARE intlastinsert INT;

INSERT INTO `t_pelunasan`(
	 int_pemeriksaan_id,
	`int_permohonan_id`,
	`int_status_pelunasan`,
	`dt_pelunasan`,
	`txt_keterangan_pelunasan`,
	`int_petugas_id`)
VALUES 
	(in_int_pemeriksaan_id,
	in_int_permohonan_id,
	1,
	in_dt_pemeriksaan_petugas,
	in_txt_keterangan,
	in_int_userid_petugas);
 
SET intlastinsert =  LAST_INSERT_ID();
 
UPDATE t_permohonan set int_progress = 3 WHERE int_permohonan_id = in_int_permohonan_id;
 
 
SELECT 1 as success,intlastinsert  as int_pelunasan_id;
 
 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_ins_datapemeriksaan` (IN `in_int_pemeriksaan_id` INT, IN `in_int_permohonan_id` INT, IN `in_txt_fungsi` TEXT, IN `in_txt_lokasi` TEXT, IN `in_num_panjang_pemeriksaan` DOUBLE(15,5), IN `in_num_lebar_pemeriksaan` DOUBLE(15,5), IN `in_num_luas_pemeriksaan` DOUBLE(15,5), IN `in_txt_latitude` TEXT, IN `in_txt_longitude` TEXT, IN `in_int_jaringan_id` INT, IN `in_txt_keterangan_pemeriksaan` TEXT, IN `in_int_userid_petugas` INT, IN `in_dt_pemeriksaan_petugas` DATE)  BEGIN
 
 DECLARE int_cekstatuspermohonan INT;

 UPDATE `t_pemeriksaan` SET 
 `txt_fungsi` = in_txt_fungsi,
 `txt_lokasi` = in_txt_lokasi,
 `num_panjang_pemeriksaan` = in_num_panjang_pemeriksaan,
 `num_lebar_pemeriksaan` = in_num_lebar_pemeriksaan,
 `num_luas_pemeriksaan` = in_num_luas_pemeriksaan,
 `txt_latitude` = in_txt_latitude,
 `txt_longitude` = in_txt_longitude,
 `int_jaringan_id` = in_int_jaringan_id,
 `txt_keterangan_pemeriksaan` = in_txt_keterangan_pemeriksaan,
 `int_userid_petugas` = in_int_userid_petugas,
 `dt_pemeriksaan_petugas` = in_dt_pemeriksaan_petugas,
 `int_valid` = 1 WHERE `int_pemeriksaan_id` = in_int_pemeriksaan_id;
 
 SET int_cekstatuspermohonan = (SELECT int_progress from t_permohonan WHERE int_permohonan_id = in_int_permohonan_id);
	
 IF  int_cekstatuspermohonan < 2 THEN
	UPDATE t_permohonan set int_progress = 1 WHERE int_permohonan_id = in_int_permohonan_id;	
 END IF;
 
 SELECT 1 as success;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_ins_fotopembayaran` (IN `in_int_pelunasan_id` INT, IN `in_txt_filename` TEXT, IN `in_txt_size` DOUBLE(8,2), IN `in_dtupload` DATE)  BEGIN

-- DECLARE txt_dir VARCHAR(200); 

-- SET txt_dir = CONCAT("/images/",in_txt_filename);

INSERT INTO `t_pelunasan_img`
	(`int_pelunasan_id`,
	`txt_direktori`,
	`txt_type`,
	`txt_size`,
	`dt_created`)
VALUES 
	(in_int_pelunasan_id,
	in_txt_filename,
	"image",
	in_txt_size,
	in_dtupload);
 
SELECT 1 as success;
 
 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_ins_fotopemeriksaan` (IN `in_int_pemeriksaan_id` INT, IN `in_txt_filename` TEXT, IN `in_txt_size` DOUBLE(8,2), IN `in_dtupload` DATE)  BEGIN

DECLARE txt_dir VARCHAR(200); 

-- SET txt_dir = CONCAT("/images/",in_txt_filename);

INSERT INTO `t_pemeriksaan_img`
	(`int_pemeriksaan_id`,
	`txt_direktori`,
	`txt_type`,
	`txt_size`,
	`dt_created`)
VALUES 
	(in_int_pemeriksaan_id,
	in_txt_filename,
	"image",
	in_txt_size,
	in_dtupload);
 
SELECT 1 as success;
 
 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_user_login` (IN `in_txtusername` VARCHAR(50), IN `in_txtpassword` VARCHAR(50))  BEGIN
 IF EXISTS(SELECT int_userid_petugas FROM m_userpetugas WHERE (txt_username = in_txtusername OR txt_email = in_txtusername) AND txt_password = in_txtpassword and is_aktif = 1)
   THEN
     SELECT 1 AS success,
								int_userid_petugas,
								txt_username,
								txt_email,
								txt_namadepan,
								txt_avatar
		 FROM m_userpetugas 
		 WHERE (txt_username = in_txtusername OR txt_email = in_txtusername) AND txt_password = in_txtpassword AND is_aktif = 1;
   ELSE
      SELECT 0 AS success, 0 AS int_userid_petugas, '' AS txt_username, '' AS txt_email, '' AS txt_namadepan, '' AS txt_avatar;
   END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `service_copydata_pemeriksaan` ()  BEGIN
 
DECLARE intYearNow INT; 
DECLARE intYearToCopy INT;
DECLARE isDataAvailableInYearNow INT;

SET intYearNow = YEAR(NOW());
SET intYearToCopy = intYearNow - 1;
SET isDataAvailableInYearNow = (SELECT COUNT(int_pemeriksaan_id) FROM t_pemeriksaan WHERE YEAR(dt_pemeriksaan_petugas) = intYearNow);

IF isDataAvailableInYearNow <= 0 THEN

INSERT INTO t_pemeriksaan (
	int_permohonan_id,
	txt_fungsi,
	txt_lokasi,
	num_panjang_pemeriksaan,
	num_lebar_pemeriksaan,
	num_luas_pemeriksaan,
	txt_latitude,
	txt_longitude,
	int_jaringan_id,
	int_tarif_fungsi_id,
	txt_no_skr,
	num_retribusi,
	dt_jatuh_tempo,
	txt_keterangan_pemeriksaan,
	int_penghitung_id,
	int_userid_petugas,
	dt_pemeriksaan_petugas,
	int_valid)
SELECT 
	int_permohonan_id,
	txt_fungsi,
	txt_lokasi,
	num_panjang_pemeriksaan,
	num_lebar_pemeriksaan,
	num_luas_pemeriksaan,
	txt_latitude,
	txt_longitude,
	int_jaringan_id,
	int_tarif_fungsi_id,
	txt_no_skr,
	num_retribusi,
	dt_jatuh_tempo,
	txt_keterangan_pemeriksaan,
	int_penghitung_id,
	int_userid_petugas,
	DATE_ADD(dt_pemeriksaan_petugas, INTERVAL 1 YEAR),
	int_valid
FROM t_pemeriksaan
WHERE YEAR(dt_pemeriksaan_petugas) = intYearToCopy AND int_valid = 2;

END IF;
 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_desa`
--

CREATE TABLE `m_desa` (
  `int_desa_id` bigint(10) NOT NULL,
  `int_kecamatan_id` int(7) DEFAULT NULL,
  `txt_desa` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_desa`
--

INSERT INTO `m_desa` (`int_desa_id`, `int_kecamatan_id`, `txt_desa`) VALUES
(3508010001, 3508010, 'TEGALREJO'),
(3508010002, 3508010, 'BULUREJO'),
(3508010003, 3508010, 'PUROREJO'),
(3508010004, 3508010, 'TEMPURREJO'),
(3508010005, 3508010, 'TEMPURSARI'),
(3508010006, 3508010, 'PUNDUNGSARI'),
(3508010007, 3508010, 'KALIULING'),
(3508020001, 3508020, 'SIDOMULYO'),
(3508020002, 3508020, 'PRONOJIWO'),
(3508020003, 3508020, 'TAMANAYU'),
(3508020004, 3508020, 'SUMBERURIP'),
(3508020005, 3508020, 'ORO ORO OMBO'),
(3508020006, 3508020, 'SUPITURANG'),
(3508030001, 3508030, 'JUGOSARI'),
(3508030002, 3508030, 'JARIT'),
(3508030003, 3508030, 'CANDIPURO'),
(3508030004, 3508030, 'SUMBERREJO'),
(3508030005, 3508030, 'SUMBERWULUH'),
(3508030006, 3508030, 'SUMBERMUJUR'),
(3508030007, 3508030, 'PENANGGAL'),
(3508030008, 3508030, 'TAMBAHREJO'),
(3508030009, 3508030, 'KLOPOSAWIT'),
(3508030010, 3508030, 'TUMPENG'),
(3508040001, 3508040, 'GONDORUSO'),
(3508040002, 3508040, 'KALIBENDO'),
(3508040003, 3508040, 'BADES'),
(3508040004, 3508040, 'BAGO'),
(3508040005, 3508040, 'SELOK AWAR AWAR'),
(3508040006, 3508040, 'CONDRO'),
(3508040007, 3508040, 'MADUREJO'),
(3508040008, 3508040, 'PASIRIAN'),
(3508040009, 3508040, 'SEMEMU'),
(3508040010, 3508040, 'NGUTER'),
(3508040011, 3508040, 'SELOK ANYAR'),
(3508050001, 3508050, 'PANDANARUM'),
(3508050002, 3508050, 'PANDANWANGI'),
(3508050003, 3508050, 'SUMBERJATI'),
(3508050004, 3508050, 'TEMPEH KIDUL'),
(3508050005, 3508050, 'LEMPENI'),
(3508050006, 3508050, 'TEMPEH TENGAH'),
(3508050007, 3508050, 'KALIWUNGU'),
(3508050008, 3508050, 'TEMPEH LOR'),
(3508050009, 3508050, 'BESUK'),
(3508050010, 3508050, 'JATISARI'),
(3508050011, 3508050, 'PULO'),
(3508050012, 3508050, 'GESANG'),
(3508050013, 3508050, 'JOKARTO'),
(3508060008, 3508060, 'BANJARWARU'),
(3508060009, 3508060, 'LABRUK LOR'),
(3508060010, 3508060, 'CITRODIWANGSAN'),
(3508060011, 3508060, 'DITOTRUNAN'),
(3508060012, 3508060, 'JOGOTRUNAN'),
(3508060013, 3508060, 'DENOK'),
(3508060014, 3508060, 'BLUKON'),
(3508060015, 3508060, 'BORENG'),
(3508060016, 3508060, 'JOGOYUDAN'),
(3508060017, 3508060, 'ROGOTRUNAN'),
(3508060018, 3508060, 'TOMPOKERSAN'),
(3508060019, 3508060, 'KEPUHARJO'),
(3508061001, 3508061, 'SUMBERSUKO'),
(3508061002, 3508061, 'KEBONSARI'),
(3508061003, 3508061, 'GRATI'),
(3508061004, 3508061, 'LABRUK KIDUL'),
(3508061005, 3508061, 'MOJOSARI'),
(3508061006, 3508061, 'SENTUL'),
(3508061007, 3508061, 'PURWOSONO'),
(3508061008, 3508061, 'PETAHUNAN'),
(3508070001, 3508070, 'WONOGRIYO'),
(3508070002, 3508070, 'WONOSARI'),
(3508070003, 3508070, 'MANGUNSARI'),
(3508070004, 3508070, 'TEKUNG'),
(3508070005, 3508070, 'WONOKERTO'),
(3508070006, 3508070, 'TUKUM'),
(3508070007, 3508070, 'KARANGBENDO'),
(3508070008, 3508070, 'KLAMPOKARUM'),
(3508080001, 3508080, 'JATIMULYO'),
(3508080002, 3508080, 'JATIREJO'),
(3508080003, 3508080, 'JATIGONO'),
(3508080004, 3508080, 'SUKOREJO'),
(3508080005, 3508080, 'SUKOSARI'),
(3508080006, 3508080, 'KUNIR KIDUL'),
(3508080007, 3508080, 'KUNIR LOR'),
(3508080008, 3508080, 'KEDUNGMORO'),
(3508080009, 3508080, 'KARANGLO'),
(3508080010, 3508080, 'KABUARAN'),
(3508080011, 3508080, 'DOROGOWOK'),
(3508090001, 3508090, 'DARUNGAN'),
(3508090002, 3508090, 'KRATON'),
(3508090003, 3508090, 'WOTGALIH'),
(3508090004, 3508090, 'TUNJUNGREJO'),
(3508090005, 3508090, 'YOSOWILANGUN KIDUL'),
(3508090006, 3508090, 'YOSOWILANGUN LOR'),
(3508090007, 3508090, 'KRAI'),
(3508090008, 3508090, 'KARANGAYAR'),
(3508090009, 3508090, 'KARANGREJO'),
(3508090010, 3508090, 'MUNDER'),
(3508090011, 3508090, 'KEBONSARI'),
(3508090012, 3508090, 'KALIPEPE'),
(3508100001, 3508100, 'NOGOSARI'),
(3508100002, 3508100, 'KEDUNGREJO'),
(3508100003, 3508100, 'SIDOREJO'),
(3508100004, 3508100, 'ROWOKANGKUNG'),
(3508100005, 3508100, 'SUMBERSARI'),
(3508100006, 3508100, 'SUMBERANYAR'),
(3508100007, 3508100, 'DAWUHAN WETAN'),
(3508110001, 3508110, 'BANYUPUTIH KIDUL'),
(3508110002, 3508110, 'ROJOPOLO'),
(3508110003, 3508110, 'SUKOSARI'),
(3508110004, 3508110, 'KALIBOTO KIDUL'),
(3508110005, 3508110, 'KALIBOTO LOR'),
(3508110006, 3508110, 'JATIROTO'),
(3508120001, 3508120, 'BANYUPUTIH LOR'),
(3508120002, 3508120, 'KALIDILEM'),
(3508120003, 3508120, 'TUNJUNG'),
(3508120004, 3508120, 'GEDANGMAS'),
(3508120005, 3508120, 'KALIPENGGUNG'),
(3508120006, 3508120, 'RANULOGONG'),
(3508120007, 3508120, 'RANDUAGUNG'),
(3508120008, 3508120, 'LEDOKTEMPURO'),
(3508120009, 3508120, 'PEJARAKAN'),
(3508120010, 3508120, 'BUWEK'),
(3508120011, 3508120, 'RANUWURUNG'),
(3508120012, 3508120, 'SALAK'),
(3508130001, 3508130, 'KLANTING'),
(3508130002, 3508130, 'KEBONAGUNG'),
(3508130003, 3508130, 'KARANGSARI'),
(3508130004, 3508130, 'DAWUHAN LOR'),
(3508130005, 3508130, 'KUTORENON'),
(3508130006, 3508130, 'SELOKBESUKI'),
(3508130007, 3508130, 'SUMBEREJO'),
(3508130008, 3508130, 'URANGGANTUNG'),
(3508130009, 3508130, 'SELOKGONDANG'),
(3508130010, 3508130, 'BONDOYUDO'),
(3508140001, 3508140, 'BARAT'),
(3508140002, 3508140, 'BABAKAN'),
(3508140003, 3508140, 'MOJO'),
(3508140004, 3508140, 'BODANG'),
(3508140005, 3508140, 'KEDAWUNG'),
(3508140006, 3508140, 'PADANG'),
(3508140007, 3508140, 'KALISEMUT'),
(3508140008, 3508140, 'MERAKAN'),
(3508140009, 3508140, 'TANGGUNG'),
(3508150001, 3508150, 'PASRUJAMBE'),
(3508150002, 3508150, 'JAMBEKUMBU'),
(3508150003, 3508150, 'SUKOREJO'),
(3508150004, 3508150, 'JAMBEARUM'),
(3508150005, 3508150, 'KERTOSARI'),
(3508150006, 3508150, 'PAGOWAN'),
(3508150007, 3508150, 'KARANGANOM'),
(3508160002, 3508160, 'PURWOREJO'),
(3508160003, 3508160, 'SARIKEMUNING'),
(3508160004, 3508160, 'PANDANSARI'),
(3508160005, 3508160, 'SENDURO'),
(3508160006, 3508160, 'BURNO'),
(3508160007, 3508160, 'KANDANGTEPUS'),
(3508160008, 3508160, 'KANDANGAN'),
(3508160009, 3508160, 'BEDAYU'),
(3508160010, 3508160, 'BEDAYUTALANG'),
(3508160011, 3508160, 'WONOCEPOKOAYU'),
(3508160012, 3508160, 'ARGOSARI'),
(3508160013, 3508160, 'RANUPANE'),
(3508170001, 3508170, 'WONOKERTO'),
(3508170002, 3508170, 'PAKEL'),
(3508170003, 3508170, 'KENONGO'),
(3508170004, 3508170, 'GUCIALIT'),
(3508170005, 3508170, 'DADAPAN'),
(3508170006, 3508170, 'KERTOWONO'),
(3508170007, 3508170, 'TUNJUNG'),
(3508170008, 3508170, 'JERUK'),
(3508170009, 3508170, 'SOMBO'),
(3508180001, 3508180, 'PANDANSARI'),
(3508180002, 3508180, 'KRASAK'),
(3508180003, 3508180, 'KEDUNGJAJANG'),
(3508180004, 3508180, 'WONOREJO'),
(3508180005, 3508180, 'UMBUL'),
(3508180006, 3508180, 'CURAHPETUNG'),
(3508180007, 3508180, 'GROBOGAN'),
(3508180008, 3508180, 'BENCE'),
(3508180009, 3508180, 'JATISARI'),
(3508180010, 3508180, 'TEMPURSARI'),
(3508180011, 3508180, 'BANDARAN'),
(3508180012, 3508180, 'SAWARAN KULON'),
(3508190001, 3508190, 'KEBONAN'),
(3508190002, 3508190, 'KUDUS'),
(3508190003, 3508190, 'DUREN'),
(3508190004, 3508190, 'SUMBERWRINGIN'),
(3508190005, 3508190, 'PAPRINGAN'),
(3508190006, 3508190, 'RANUPAKIS'),
(3508190007, 3508190, 'TEGALRANDU'),
(3508190008, 3508190, 'KLAKAH'),
(3508190009, 3508190, 'MLAWANG'),
(3508190010, 3508190, 'SRUNI'),
(3508190011, 3508190, 'TEGALCIUT'),
(3508190012, 3508190, 'SAWARAN LOR'),
(3508200001, 3508200, 'JENGGRONG'),
(3508200002, 3508200, 'MENINJO'),
(3508200003, 3508200, 'TEGALBANGSRI'),
(3508200004, 3508200, 'SUMBERPETUNG'),
(3508200005, 3508200, 'ALUN ALUN'),
(3508200006, 3508200, 'RANUBEDALI'),
(3508200007, 3508200, 'RANUYOSO'),
(3508200008, 3508200, 'WONOAYU'),
(3508200009, 3508200, 'PENAWUNGAN'),
(3508200010, 3508200, 'WATES KULON'),
(3508200011, 3508200, 'WATES WETAN');

-- --------------------------------------------------------

--
-- Table structure for table `m_jabatan`
--

CREATE TABLE `m_jabatan` (
  `int_jabatan_id` int(11) NOT NULL,
  `txt_jabatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `m_jaringan`
--

CREATE TABLE `m_jaringan` (
  `int_jaringan_id` int(11) NOT NULL,
  `txt_kode` varchar(30) DEFAULT NULL,
  `txt_jaringan` varchar(50) DEFAULT NULL,
  `dbl_latitude` double(11,8) DEFAULT NULL,
  `dbl_longitude` double(11,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_jaringan`
--

INSERT INTO `m_jaringan` (`int_jaringan_id`, `txt_kode`, `txt_jaringan`, `dbl_latitude`, `dbl_longitude`) VALUES
(2, '335080052', 'Bacem I', NULL, NULL),
(3, '-', 'Bacem II', NULL, NULL),
(4, '-', 'Banjarejo', NULL, NULL),
(5, '335080174', 'Bayur', NULL, NULL),
(6, '335080064', 'Bedayu', NULL, NULL),
(7, '335080061', 'Bendo', NULL, NULL),
(8, '335080050', 'Bentrong Suko', NULL, NULL),
(9, '335080130', 'Besuki', NULL, NULL),
(10, '335080221', 'Betoto I', NULL, NULL),
(11, '335080031', 'Betoto II', NULL, NULL),
(12, '335080043', 'Bisri', NULL, NULL),
(13, '335080122', 'Blok Rejo', NULL, NULL),
(14, '335080003', 'Blukon', NULL, NULL),
(15, '335080042', 'Bodang', NULL, NULL),
(16, '335080024', 'Boreng', NULL, NULL),
(17, '335080028', 'Boto', NULL, NULL),
(18, '335080149', 'Bulak Klakah', NULL, NULL),
(19, '335080223', 'Bulak Manggis', NULL, NULL),
(20, '335080150', 'Candri', NULL, NULL),
(21, '335080267', 'Cangkring I', NULL, NULL),
(22, '335080224', 'Cangkring II', NULL, NULL),
(23, '335080197', 'Carang Kuning', NULL, NULL),
(24, '-', 'Celeng Mati', NULL, NULL),
(25, '335080209', 'Curah Petung I', NULL, NULL),
(26, '-', 'Curah Petung II', NULL, NULL),
(27, '335080129', 'Curah Wedi I', NULL, NULL),
(28, '335080107', 'Curah Wedi II', NULL, NULL),
(29, '335080086', 'Dadapan', NULL, NULL),
(30, '335080170', 'Darungan', NULL, NULL),
(31, '335080070', 'Demo', NULL, NULL),
(32, '335080072', 'Depok', NULL, NULL),
(33, '335080013', 'Dilem', NULL, NULL),
(34, '-', 'Djatim', NULL, NULL),
(35, '-', 'Dompyong I', NULL, NULL),
(36, '-', 'Dompyong II', NULL, NULL),
(37, '335080006', 'Duk', NULL, NULL),
(38, '335080125', 'Duk I', NULL, NULL),
(39, '335080082', 'Duk II', NULL, NULL),
(40, '335080228', 'Duk III', NULL, NULL),
(41, '335080229', 'Duk IV', NULL, NULL),
(42, '335080226', 'Duk V', NULL, NULL),
(43, '335080059', 'Dul Mungit', NULL, NULL),
(44, '335080131', 'Durek', NULL, NULL),
(45, '335080187', 'Duren I', NULL, NULL),
(46, '335080117', 'Duren II', NULL, NULL),
(47, '335080233', 'Duren III', NULL, NULL),
(48, '335080231', 'Duren IV', NULL, NULL),
(49, '335080232', 'Duren V', NULL, NULL),
(50, '335080078', 'Duren VI', NULL, NULL),
(51, '335080065', 'Gedang Mas', NULL, NULL),
(52, '335080045', 'Gedang Mas I', NULL, NULL),
(53, '335080009', 'Gedang Mas II', NULL, NULL),
(54, '335080134', 'Gedangan', NULL, NULL),
(55, '335080234', 'Gempol', NULL, NULL),
(56, '335080001', 'Gubug Domas', NULL, NULL),
(57, '335080151', 'Ismail', NULL, NULL),
(58, '335080196', 'Jabon', NULL, NULL),
(59, '335080181', 'Jagalan ', NULL, NULL),
(60, '335080145', 'Jagalan I', NULL, NULL),
(61, '335080169', 'Jamal', NULL, NULL),
(62, '-', 'Jambe', NULL, NULL),
(63, '335080087', 'Jati', NULL, NULL),
(64, '335080236', 'Jemuit', NULL, NULL),
(65, '335080315', 'Jerukan/Monisa', NULL, NULL),
(66, '335080237', 'Jombok I', NULL, NULL),
(67, '335080235', 'Jombok II', NULL, NULL),
(68, '335080238', 'Jombok III', NULL, NULL),
(69, '335080051', 'Jonggrang I', NULL, NULL),
(70, '335080166', 'Jonggrang II', NULL, NULL),
(71, '335080178', 'Jonggrang III', NULL, NULL),
(72, '335080239', 'Jonggrang IV', NULL, NULL),
(73, '-', 'Kali Wuluh', NULL, NULL),
(74, '335080245', 'Kali Piri', NULL, NULL),
(75, '335080184', 'Kamid', NULL, NULL),
(76, '335080157', 'Karang Anyar I', NULL, NULL),
(77, '335080165', 'Karang Anyar II', NULL, NULL),
(78, '335080088', 'Karang Culik', NULL, NULL),
(79, '335080240', 'Karang Kletak', NULL, NULL),
(80, '335080143', 'Kebon Deli I', NULL, NULL),
(81, '335080108', 'Kebon Deli II', NULL, NULL),
(82, '335080141', 'Kebon Deli III', NULL, NULL),
(83, '335080139', 'Kebon Deli IV', NULL, NULL),
(84, '335080222', 'Kebon Deli V', NULL, NULL),
(85, '335080016', 'Kedung Caring', NULL, NULL),
(86, '335080217', 'Kedung Klampok', NULL, NULL),
(87, '335080215', 'Kedung Lier I', NULL, NULL),
(88, '335080241', 'Kedung Lier II', NULL, NULL),
(89, '-', 'Kemiri I', NULL, NULL),
(90, '-', 'Kemiri II', NULL, NULL),
(91, '335080010', 'Klakah Pakis', NULL, NULL),
(92, '335080002', 'Klerek', NULL, NULL),
(93, '335080074', 'Klopogading', NULL, NULL),
(94, '335080056', 'Kowang', NULL, NULL),
(95, '335080023', 'Krai', NULL, NULL),
(96, '335080096', 'Krumbang I', NULL, NULL),
(97, '335080295', 'Krumbang II', NULL, NULL),
(98, '335080161', 'Krumbang III', NULL, NULL),
(99, '335080097', 'Krumbang IV', NULL, NULL),
(100, '335080152', 'Kuburan', NULL, NULL),
(101, '335080101', 'Lalangan', NULL, NULL),
(102, '335080242', 'Lancing', NULL, NULL),
(103, '335080138', 'Langgar I', NULL, NULL),
(104, '335080123', 'Langgar II', NULL, NULL),
(105, '335080073', 'Langgar III / BT', NULL, NULL),
(106, '335080190', 'Langkapan', NULL, NULL),
(107, '335080243', 'Lateng I', NULL, NULL),
(108, '335080055', 'Lateng II', NULL, NULL),
(109, '335080142', 'Legenan', NULL, NULL),
(110, '335080159', 'Lewung I', NULL, NULL),
(111, '335080112', 'Lewung II', NULL, NULL),
(112, '335080038', 'Lobang Kanan', NULL, NULL),
(113, '335080008', 'Lobang Kiri', NULL, NULL),
(114, '335080120', 'Lombok', NULL, NULL),
(115, '335080244', 'Mangun', NULL, NULL),
(116, '335080172', 'Mian', NULL, NULL),
(117, '335080113', 'Mualap', NULL, NULL),
(118, '335080205', 'Ngrepyak', NULL, NULL),
(119, '335080155', 'Pahlewen', NULL, NULL),
(120, '-', 'Pak Duan', NULL, NULL),
(121, '335080126', 'Pak Jum', NULL, NULL),
(122, '335080193', 'Pakel I', NULL, NULL),
(123, '335080104', 'Pakel II', NULL, NULL),
(124, '335080089', 'Pakurejo', NULL, NULL),
(125, '335080025', 'Paleran', NULL, NULL),
(126, '335080185', 'Pancing I', NULL, NULL),
(127, '335080250', 'Pancing II', NULL, NULL),
(128, '335080249', 'Pancing III', NULL, NULL),
(129, '335080248', 'Pancing IV', NULL, NULL),
(130, '335080066', 'Pandanwangi I', NULL, NULL),
(131, '335080017', 'Pandanwangi II', NULL, NULL),
(132, '335080090', 'Panggung', NULL, NULL),
(133, '335080111', 'Pelus I', NULL, NULL),
(134, '335080114', 'Pelus II', NULL, NULL),
(135, '-', 'Pelus III', NULL, NULL),
(136, '335080252', 'Petehan', NULL, NULL),
(137, '-', 'Pinus', NULL, NULL),
(138, '335080102', 'Poh I', NULL, NULL),
(139, '335080186', 'Poh II', NULL, NULL),
(140, '335080254', 'Poh III', NULL, NULL),
(141, '335080247', 'Poh IV', NULL, NULL),
(142, '335080246', 'Poh V', NULL, NULL),
(143, '335080253', 'Poh VI', NULL, NULL),
(144, '335080255', 'Ponco Kusumo', NULL, NULL),
(145, '-', 'Prapatan', NULL, NULL),
(146, '335080034', 'Pringtali', NULL, NULL),
(147, '335080092', 'Rahayu', NULL, NULL),
(148, '335080037', 'Rakinten', NULL, NULL),
(149, '335080020', 'Rawaan', NULL, NULL),
(150, '335080080', 'Regoyo', NULL, NULL),
(151, '335080012', 'Rejali', NULL, NULL),
(152, '335080176', 'Rekesan I', NULL, NULL),
(153, '335080257', 'Rekesan II', NULL, NULL),
(154, '335080115', 'Repeh', NULL, NULL),
(155, '335080258', 'Rowo Asin', NULL, NULL),
(156, '335080004', 'Rowo Gedang', NULL, NULL),
(157, '335080259', 'Sadap Sentul', NULL, NULL),
(158, '335080079', 'Sapari', NULL, NULL),
(159, '335080039', 'Sarijo', NULL, NULL),
(160, '335080029', 'Selok Awar-awar', NULL, NULL),
(161, '335080014', 'Selokambang ', NULL, NULL),
(162, '335080005', 'Sempu I', NULL, NULL),
(163, '335080077', 'Sempu II', NULL, NULL),
(164, '335080147', 'Semut I', NULL, NULL),
(165, '335080154', 'Semut II', NULL, NULL),
(166, '335080171', 'Semut III', NULL, NULL),
(167, '335080144', 'Sengon', NULL, NULL),
(168, '335080040', 'Solekan', NULL, NULL),
(169, '335080011', 'Soponyono', NULL, NULL),
(170, '335080068', 'Srenteng', NULL, NULL),
(171, '335080105', 'Suco', NULL, NULL),
(172, '335080035', 'Sukorejo', NULL, NULL),
(173, '335080270', 'Sumber Banji', NULL, NULL),
(174, '335080263', 'Sumber Baung', NULL, NULL),
(175, '335080036', 'Sumber Bendo', NULL, NULL),
(176, '335080183', 'Sumber Bening', NULL, NULL),
(177, '335080262', 'Sumber Bentrong', NULL, NULL),
(178, '335080214', 'Sumber Berca', NULL, NULL),
(179, '335080210', 'Sumber Biting', NULL, NULL),
(180, '335080093', 'Sumber Bopong', NULL, NULL),
(181, '335080071', 'Sumber Brubi', NULL, NULL),
(182, '335080109', 'Sumber Bulak Wareng', NULL, NULL),
(183, '335080148', 'Sumber Bulus', NULL, NULL),
(184, '335080264', 'Sumber Bunting', NULL, NULL),
(185, '335080146', 'Sumber Buntung ', NULL, NULL),
(186, '335080266', 'Sumber Buntung II', NULL, NULL),
(187, '335080203', 'Sumber Buntung III', NULL, NULL),
(188, '335080195', 'Sumber Bunyu ', NULL, NULL),
(189, '335080094', 'Sumber Cabek I', NULL, NULL),
(190, '335080216', 'Sumber Cabek II', NULL, NULL),
(191, '335080225', 'Sumber Cemeng', NULL, NULL),
(192, '335080268', 'Sumber Cilik', NULL, NULL),
(193, '335080261', 'Sumber Ciri', NULL, NULL),
(194, '335080269', 'Sumber Dadi', NULL, NULL),
(195, '-', 'Sumber Dairun', NULL, NULL),
(196, '335080271', 'Sumber Dluwuk', NULL, NULL),
(197, '335080272', 'Sumber Dongki', NULL, NULL),
(198, '335080273', 'Sumber Dukuh', NULL, NULL),
(199, '335080135', 'Sumber Gebang', NULL, NULL),
(200, '335080207', 'Sumber Gebang', NULL, NULL),
(201, '335080058', 'Sumber Gebang', NULL, NULL),
(202, '-', 'Sumber Gedong Sutro', NULL, NULL),
(203, '335080060', 'Sumber Gendol', NULL, NULL),
(204, '335080033', 'Sumber Gentong', NULL, NULL),
(205, '335080162', 'Sumber Gentong', NULL, NULL),
(206, '335080303', 'Sumber Gentong', NULL, NULL),
(207, '335080191', 'Sumber Gledek', NULL, NULL),
(208, '335080132', 'Sumber Glendangan', NULL, NULL),
(209, '335080046', 'Sumber Glintungan', NULL, NULL),
(210, '335080018', 'Sumber Gogosan', NULL, NULL),
(211, '335080202', 'Sumber Gotehan', NULL, NULL),
(212, '335080116', 'Sumber Grimis', NULL, NULL),
(213, '335080098', 'Sumber Guci', NULL, NULL),
(214, '335080156', 'Sumber Gumuk Mas', NULL, NULL),
(215, '335080200', 'Sumber Jagal', NULL, NULL),
(216, '335080106', 'Sumber Jajang I', NULL, NULL),
(217, '335080140', 'Sumber Jajang II', NULL, NULL),
(218, '335080081', 'Sumber Jajang III', NULL, NULL),
(219, '335080049', 'Sumber Jambe', NULL, NULL),
(220, '335080168', 'Sumber Jebok', NULL, NULL),
(221, '335080032', 'Sumber Jengger', NULL, NULL),
(222, '335080137', 'Sumber Kadi', NULL, NULL),
(223, '335080275', 'Sumber Kajar Kuning', NULL, NULL),
(224, '335080076', 'Sumber Kaliputih', NULL, NULL),
(225, '335080277', 'Sumber Kalong', NULL, NULL),
(226, '335080278', 'Sumber Kalong', NULL, NULL),
(227, '335080103', 'Sumber Kamar A', NULL, NULL),
(228, '335080279', 'Sumber Kecambil', NULL, NULL),
(229, '335080201', 'Sumber Kedawung', NULL, NULL),
(230, '-', 'Sumber Kemanten', NULL, NULL),
(231, '335080189', 'Sumber Kembang', NULL, NULL),
(232, '335080160', 'Sumber Kembar', NULL, NULL),
(233, '-', 'Sumber Kenek', NULL, NULL),
(234, '335080048', 'Sumber Kepuh', NULL, NULL),
(235, '335080053', 'Sumber Khutuk', NULL, NULL),
(236, '335080127', 'Sumber Klinting', NULL, NULL),
(237, '335080136', 'Sumber Klutuk', NULL, NULL),
(238, '335080276', 'Sumber Kokap', NULL, NULL),
(239, '335080133', 'Sumber Kolbek', NULL, NULL),
(240, '335080280', 'Sumber Kramat', NULL, NULL),
(241, '335080095', 'Sumber Kuning', NULL, NULL),
(242, '335080015', 'Sumber Kutuk', NULL, NULL),
(243, '335080281', 'Sumber Ledok', NULL, NULL),
(244, '335080282', 'Sumber Lengkong I', NULL, NULL),
(245, '335080175', 'Sumber Lengkong II', NULL, NULL),
(246, '335080163', 'Sumber Logong', NULL, NULL),
(247, '335080164', 'Sumber Lowo', NULL, NULL),
(248, '-', 'Sumber Manggarai', NULL, NULL),
(249, '335080194', 'Sumber Mijah', NULL, NULL),
(250, '335080283', 'Sumber Mojo', NULL, NULL),
(251, '335080284', 'Sumber Mualam', NULL, NULL),
(252, '335080022', 'Sumber Mujur', NULL, NULL),
(253, '335080285', 'Sumber Munder', NULL, NULL),
(254, '335080099', 'Sumber Muris', NULL, NULL),
(255, '-', 'Sumber Nah', NULL, NULL),
(256, '335080199', 'Sumber Ngambon', NULL, NULL),
(257, '335080128', 'Sumber Nongko I', NULL, NULL),
(258, '335080177', 'Sumber Nongko II', NULL, NULL),
(259, '335080286', 'Sumber Ojo Lali', NULL, NULL),
(260, '335080314', 'Sumber Onggomanik', NULL, NULL),
(261, '335080100', 'Sumber Pakel', NULL, NULL),
(262, '335080062', 'Sumber Pakem', NULL, NULL),
(263, '335080047', 'Sumber Pakis', NULL, NULL),
(264, '335080251', 'Sumber Pancoran', NULL, NULL),
(265, '335080212', 'Sumber Pandan', NULL, NULL),
(266, '335080287', 'Sumber Parang', NULL, NULL),
(267, '335080288', 'Sumber Penanggungan', NULL, NULL),
(268, '335080204', 'Sumber Penggung', NULL, NULL),
(269, '335080304', 'Sumber Pentung Sumur', NULL, NULL),
(270, '335080085', 'Sumber Persam', NULL, NULL),
(271, '335080211', 'Sumber Petung', NULL, NULL),
(272, '335080289', 'Sumber Poh', NULL, NULL),
(273, '335080179', 'Sumber Pule', NULL, NULL),
(274, '335080030', 'Sumber Puring', NULL, NULL),
(275, '335080063', 'Sumber Putri', NULL, NULL),
(276, '335080256', 'Sumber Rambak Pakis', NULL, NULL),
(277, '335080290', 'Sumber Rampal', NULL, NULL),
(278, '335080274', 'Sumber Ranu Glabak', NULL, NULL),
(279, '-', 'Sumber Ranu Wurung', NULL, NULL),
(280, '335080292', 'Sumber Ranuyoso', NULL, NULL),
(281, '335080291', 'Sumber Rojo', NULL, NULL),
(282, '335080173', 'Sumber Rowo', NULL, NULL),
(283, '335080192', 'Sumber Rowo', NULL, NULL),
(284, '335080293', 'Sumber Rowo Baung', NULL, NULL),
(285, '335080213', 'Sumber Rowo Kancu', NULL, NULL),
(286, '335080294', 'Sumber Rowosumo', NULL, NULL),
(287, '335080121', 'Sumber Salak', NULL, NULL),
(288, '-', 'Sumber Sari', NULL, NULL),
(289, '335080041', 'Sumber Sayang', NULL, NULL),
(290, '335080260', 'Sumber Sebono', NULL, NULL),
(291, '335080118', 'Sumber Sengon', NULL, NULL),
(292, '335080296', 'Sumber Sentul', NULL, NULL),
(293, '335080297', 'Sumber Sepikul', NULL, NULL),
(295, '335080019', 'Sumber Sinonggo', NULL, NULL),
(296, '335080198', 'Sumber Sintok', NULL, NULL),
(297, '335080299', 'Sumber Sonok', NULL, NULL),
(298, '335080091', 'Sumber Suko', NULL, NULL),
(299, '335080208', 'Sumber Suko', NULL, NULL),
(300, '-', 'Sumber Sukosari', NULL, NULL),
(301, '335080300', 'Sumber Sumberan', NULL, NULL),
(302, '335080057', 'Sumber Takir', NULL, NULL),
(303, '335080153', 'Sumber Tangis', NULL, NULL),
(304, '335080301', 'Sumber Tangkil', NULL, NULL),
(305, '335080206', 'Sumber Tekik', NULL, NULL),
(306, '335080182', 'Sumber Tembok', NULL, NULL),
(307, '335080119', 'Sumber Temo', NULL, NULL),
(308, '335080302', 'Sumber Tempuran', NULL, NULL),
(309, '335080110', 'Sumber Tretes', NULL, NULL),
(310, '-', 'Sumber Tumpuk/Tunjung', NULL, NULL),
(311, '335080167', 'Sumber Umbulan', NULL, NULL),
(312, '335080305', 'Sumber Urang', NULL, NULL),
(313, '335080158', 'Sumber Urip', NULL, NULL),
(314, '335080180', 'Sumber Wakap', NULL, NULL),
(315, '335080307', 'Sumber Wongso', NULL, NULL),
(316, '335080308', 'Sumber Wringin', NULL, NULL),
(317, '335080054', 'Sumberan', NULL, NULL),
(318, '335080084', 'Tabon', NULL, NULL),
(319, '335080026', 'Talang', NULL, NULL),
(320, '335080124', 'Tambuh', NULL, NULL),
(321, '335080309', 'Tandak', NULL, NULL),
(322, '335080313', 'Temari', NULL, NULL),
(323, '-', 'Tino', NULL, NULL),
(324, '335080067', 'Tong Maling', NULL, NULL),
(325, '335080188', 'Umbul Sari I ', NULL, NULL),
(326, '335080044', 'Umbul Sari II', NULL, NULL),
(327, '335080306', 'Wakap', NULL, NULL),
(328, '335080311', 'Wareng', NULL, NULL),
(329, '335080312', 'Wonokerto', NULL, NULL),
(332, '131232', 'erwerwe', 356.00000000, 546.00000000);

-- --------------------------------------------------------

--
-- Table structure for table `m_kecamatan`
--

CREATE TABLE `m_kecamatan` (
  `int_kecamatan_id` int(7) NOT NULL,
  `int_kabupaten_id` int(4) NOT NULL,
  `txt_kecamatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_kecamatan`
--

INSERT INTO `m_kecamatan` (`int_kecamatan_id`, `int_kabupaten_id`, `txt_kecamatan`) VALUES
(3508010, 3508, 'TEMPURSARI'),
(3508020, 3508, 'PRONOJIWO'),
(3508030, 3508, 'CANDIPURO'),
(3508040, 3508, 'PASIRIAN'),
(3508050, 3508, 'TEMPEH'),
(3508060, 3508, 'LUMAJANG'),
(3508061, 3508, 'SUMBERSUKO'),
(3508070, 3508, 'TEKUNG'),
(3508080, 3508, 'KUNIR'),
(3508090, 3508, 'YOSOWILANGUN'),
(3508100, 3508, 'ROWOKANGKUNG'),
(3508110, 3508, 'JATIROTO'),
(3508120, 3508, 'RANDUAGUNG'),
(3508130, 3508, 'SUKODONO'),
(3508140, 3508, 'PADANG'),
(3508150, 3508, 'PASRUJAMBE'),
(3508160, 3508, 'SENDURO'),
(3508170, 3508, 'GUCIALIT'),
(3508180, 3508, 'KEDUNGJAJANG'),
(3508190, 3508, 'KLAKAH'),
(3508200, 3508, 'RANUYOSO');

-- --------------------------------------------------------

--
-- Table structure for table `m_pegawai`
--

CREATE TABLE `m_pegawai` (
  `int_pegawai_id` int(11) NOT NULL,
  `int_jabatan_id` int(11) DEFAULT NULL,
  `txt_nama_pejabat` varchar(50) DEFAULT NULL,
  `txt_nip` text,
  `is_aktif` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_pegawai`
--

INSERT INTO `m_pegawai` (`int_pegawai_id`, `int_jabatan_id`, `txt_nama_pejabat`, `txt_nip`, `is_aktif`) VALUES
(1, 1, 'Rama Yana', '0998877983 878 1', NULL),
(2, 1, 'Gatot Kaca', '1319873913719 871 1', NULL),
(3, 1, 'Gareng', '989709809809 123 8', NULL),
(4, 2, 'Gajah Mada', '9999999999999 999 9', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_tarif_fungsi`
--

CREATE TABLE `m_tarif_fungsi` (
  `int_tarif_fungsi_id` int(11) NOT NULL,
  `txt_tarif_fungsi` varchar(50) DEFAULT NULL,
  `cur_tarif_fungsi` double(15,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_tarif_fungsi`
--

INSERT INTO `m_tarif_fungsi` (`int_tarif_fungsi_id`, `txt_tarif_fungsi`, `cur_tarif_fungsi`) VALUES
(1, 'Rumah Permanen', 7500.00000),
(2, 'Rumah Semi Permanen', 1000.00000),
(3, 'Rumah Sederhana', 5000.00000);

-- --------------------------------------------------------

--
-- Table structure for table `m_upt`
--

CREATE TABLE `m_upt` (
  `int_upt_id` int(11) NOT NULL,
  `txt_nama_upt` varchar(50) DEFAULT NULL,
  `int_kecamatan_id` int(11) DEFAULT NULL,
  `txt_alamat` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_upt`
--

INSERT INTO `m_upt` (`int_upt_id`, `txt_nama_upt`, `int_kecamatan_id`, `txt_alamat`) VALUES
(2, 'gucialit', 3508110, 'werwer4e');

-- --------------------------------------------------------

--
-- Table structure for table `m_upt_akses_kecamatan`
--

CREATE TABLE `m_upt_akses_kecamatan` (
  `int_upt_akses_kecamatan_id` int(11) NOT NULL,
  `int_upt_id` int(11) DEFAULT NULL,
  `int_kecamatan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `intIDuser` int(11) NOT NULL,
  `txtUsername` varchar(50) DEFAULT NULL,
  `txtEmail` varchar(50) DEFAULT NULL,
  `txtPassword` varchar(60) DEFAULT NULL,
  `txtFirstName` varchar(50) DEFAULT NULL,
  `txtLastName` varchar(50) DEFAULT NULL,
  `txtAddress` varchar(100) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL COMMENT '0 = NonAktif , 1 = aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`intIDuser`, `txtUsername`, `txtEmail`, `txtPassword`, `txtFirstName`, `txtLastName`, `txtAddress`, `isActive`) VALUES
(1, 'admin', 'admin@gmail.com', '73ce049dd94e30c575a8b4a7a6b25a6a', 'admin', 'last name admin', 'jalan merdeka no 45', 1),
(6, 'zawa', 'zawaruddin017@gmail.com', '$2y$10$szwVEvWCN0wCtSqPK9XT0.3pwgxPwYtDZ06amrYZAuS', 'zawa', 'zawa', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_userpetugas`
--

CREATE TABLE `m_userpetugas` (
  `int_userid_petugas` int(11) NOT NULL,
  `txt_namadepan` varchar(50) DEFAULT NULL,
  `txt_namabelakang` varchar(50) DEFAULT NULL,
  `txt_alamat` varchar(255) DEFAULT NULL,
  `txt_email` varchar(50) DEFAULT NULL,
  `txt_username` varchar(50) DEFAULT NULL,
  `txt_password` varchar(60) DEFAULT NULL,
  `txt_avatar` varchar(50) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `dt_created` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_userpetugas`
--

INSERT INTO `m_userpetugas` (`int_userid_petugas`, `txt_namadepan`, `txt_namabelakang`, `txt_alamat`, `txt_email`, `txt_username`, `txt_password`, `txt_avatar`, `is_aktif`, `dt_created`) VALUES
(1, 'Test', 'Pkd', 'Jl. Mertojoyo Selatan', 'pkd@pkdmail.com', 'pkd', '$2y$10$cTWwGiAYDWaz9i3NpTN4geqrM/hZOGaEzT51LEik6g5JpYC.KSVBe', 'default.png', 1, '2020-07-01 20:19:06.000000'),
(3, 'danang', 'subandriyo', NULL, NULL, 'danang', '123456', NULL, 1, '2020-05-13 11:36:58.000000');

-- --------------------------------------------------------

--
-- Table structure for table `m_userpetugas_akses`
--

CREATE TABLE `m_userpetugas_akses` (
  `int_userid_petugas_akses` int(11) NOT NULL,
  `int_userid_petugas` int(11) DEFAULT NULL,
  `int_kecamatan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_userpetugas_akses`
--

INSERT INTO `m_userpetugas_akses` (`int_userid_petugas_akses`, `int_userid_petugas`, `int_kecamatan_id`) VALUES
(1, 1, 3508050),
(2, 1, 3508020),
(3, 1, 3508190),
(4, 1, 3508030);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin`
--

CREATE TABLE `m_user_admin` (
  `intIDuser` int(11) NOT NULL,
  `txtNamaDepan` varchar(100) DEFAULT NULL,
  `txtNamaBelakang` varchar(100) DEFAULT NULL,
  `txtUsername` varchar(50) DEFAULT NULL,
  `txtPassword` varchar(255) DEFAULT NULL,
  `intStatus` int(11) DEFAULT NULL,
  `dtRegistrasi` datetime DEFAULT CURRENT_TIMESTAMP,
  `intIDGroup` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_user_admin`
--

INSERT INTO `m_user_admin` (`intIDuser`, `txtNamaDepan`, `txtNamaBelakang`, `txtUsername`, `txtPassword`, `intStatus`, `dtRegistrasi`, `intIDGroup`) VALUES
(1, 'Super', 'Administrator', 'super', '$2y$10$BwpDX6cBkIkxCqAGDHW8AOzc/TjAOC19gV5OqQ8gsCkkZ44QgT0Ge', 1, '2019-11-11 20:40:54', 1),
(2, 'Administrator', '', 'admin', '$2y$10$Jhh7x236NXGL4thBxczADes.3ZbKL4Mi1shJnTaEHfzMd7x4iaTIC', 1, '2020-05-08 13:14:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin_akses_upt`
--

CREATE TABLE `m_user_admin_akses_upt` (
  `intIDUserAksesAdminUPT` int(11) NOT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `intIDUPT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin_group`
--

CREATE TABLE `m_user_admin_group` (
  `intIDGroup` int(11) NOT NULL,
  `txtNama` varchar(50) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_user_admin_group`
--

INSERT INTO `m_user_admin_group` (`intIDGroup`, `txtNama`, `isActive`) VALUES
(1, 'Super Admin', 1),
(2, 'Administrator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin_group_menu`
--

CREATE TABLE `m_user_admin_group_menu` (
  `intID` int(11) NOT NULL,
  `intIDGroup` int(11) DEFAULT NULL,
  `intIDMenu` int(11) DEFAULT NULL,
  `c` tinyint(4) DEFAULT '0',
  `r` tinyint(4) DEFAULT '0',
  `u` tinyint(4) DEFAULT '0',
  `d` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_user_admin_group_menu`
--

INSERT INTO `m_user_admin_group_menu` (`intID`, `intIDGroup`, `intIDMenu`, `c`, `r`, `u`, `d`) VALUES
(152, 2, 0, 0, 0, 0, 0),
(153, 2, 0, 0, 0, 0, 0),
(154, 2, 1, 1, 1, 0, 0),
(155, 2, 2, 0, 1, 0, 0),
(156, 2, 9, 1, 1, 0, 1),
(157, 2, 10, 1, 1, 0, 1),
(158, 2, 11, 1, 1, 0, 1),
(159, 2, 12, 1, 1, 0, 1),
(160, 2, 13, 1, 1, 0, 1),
(161, 2, 3, 0, 1, 0, 0),
(162, 2, 14, 1, 1, 1, 1),
(163, 2, 15, 1, 1, 1, 1),
(164, 2, 16, 1, 1, 1, 1),
(165, 2, 17, 1, 1, 1, 1),
(166, 2, 18, 1, 1, 1, 1),
(167, 2, 19, 1, 1, 1, 1),
(168, 2, 20, 1, 1, 1, 1),
(169, 2, 21, 1, 1, 1, 1),
(170, 2, 4, 0, 1, 0, 0),
(171, 2, 5, 1, 1, 1, 1),
(172, 2, 6, 1, 1, 0, 0),
(173, 2, 7, 1, 1, 0, 0),
(174, 2, 8, 1, 1, 0, 0),
(232, 1, 0, 0, 0, 0, 0),
(233, 1, 0, 0, 0, 0, 0),
(234, 1, 0, 0, 0, 0, 0),
(235, 1, 0, 0, 0, 0, 0),
(236, 1, 23, 1, 1, 1, 1),
(237, 1, 22, 1, 1, 1, 1),
(238, 1, 24, 1, 1, 1, 1),
(239, 1, 3, 0, 1, 0, 0),
(240, 1, 15, 1, 1, 1, 1),
(241, 1, 25, 1, 1, 1, 1),
(242, 1, 26, 1, 1, 1, 1),
(243, 1, 21, 1, 1, 1, 1),
(244, 1, 4, 0, 1, 0, 0),
(245, 1, 5, 1, 1, 1, 1),
(246, 1, 6, 1, 1, 1, 1),
(247, 1, 7, 1, 1, 1, 1),
(248, 1, 8, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin_menu`
--

CREATE TABLE `m_user_admin_menu` (
  `intIdMenu` int(11) NOT NULL,
  `txtKode` varchar(25) DEFAULT NULL,
  `txtNama` varchar(50) DEFAULT NULL,
  `txtUrl` varchar(255) DEFAULT NULL,
  `intLevel` int(11) DEFAULT NULL,
  `intUrutan` int(11) DEFAULT NULL,
  `intParentId` int(11) DEFAULT NULL,
  `txtClass` varchar(25) DEFAULT NULL,
  `txtIcon` varchar(50) DEFAULT NULL,
  `isActive` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_user_admin_menu`
--

INSERT INTO `m_user_admin_menu` (`intIdMenu`, `txtKode`, `txtNama`, `txtUrl`, `intLevel`, `intUrutan`, `intParentId`, `txtClass`, `txtIcon`, `isActive`) VALUES
(1, 'DASHBOARD', 'Dashboard', '/', 1, 1, NULL, 'dashboard', 'fas fa-tachometer-alt', 1),
(3, 'MASTER', 'Data Master', NULL, 1, 8, NULL, 'master', 'fas fa-th', 1),
(4, 'SYSTEM', 'Sistem', NULL, 1, 9, NULL, 'sistem', 'fas fa-cogs', 1),
(5, 'USER-SURVEY', 'Petugas Survey', 's_surveyor', 2, 91, 4, 's_surveyor', 'fas fa-minus text-xs', 1),
(6, 'USER-ADMIN', 'User Admin', 's_user', 2, 92, 4, 's_user', 'fas fa-minus text-xs', 1),
(7, 'MENU', 'Menu', 's_menu', 2, 93, 4, 's_menu', 'fas fa-minus text-xs', 1),
(8, 'GROUP', 'Group', 's_group', 2, 94, 4, 's_group', 'fas fa-minus text-xs', 1),
(15, 'IRRIGATION-NETWORK', 'Jaringan Irigasi', 'm_network', 2, 21, 3, 'm_network', 'far fa-circle nav-icon', 1),
(21, 'MASTER-UPT', 'UPT', 'm_upt', 2, 24, 3, 'm_upt', 'far fa-circle nav-icon', 1),
(22, 'PEMERIKSAAN', 'Pemeriksaan', 'pemeriksaan', 1, 3, NULL, 'pemeriksaan', 'fas fa-edit', 1),
(23, 'PERMOHONAN', 'Permohonan', 'permohonan', 1, 2, NULL, 'permohonan', 'fas fa-file-alt', 1),
(24, 'RETRIBUSI', 'Retribusi', 'retribusi', 1, 4, NULL, 'retribusi', 'fas fa-receipt', 1),
(25, 'TARIF', 'Tarif Fungsi', 'm_tarif', 2, 22, 3, 'm_tarif', 'far fa-circle nav-icon', 1),
(26, 'PEGAWAI', 'Pejabat Retribusi', 'm_pegawai', 2, 23, 3, 'm_pegawai', 'far fa-circle nav-icon', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_pelunasan`
--

CREATE TABLE `t_pelunasan` (
  `int_pelunasan_id` int(3) NOT NULL,
  `int_permohonan_id` int(11) DEFAULT NULL,
  `int_pemeriksaan_id` int(11) DEFAULT NULL,
  `int_status_pelunasan` int(1) DEFAULT NULL,
  `dt_pelunasan` datetime(6) DEFAULT NULL,
  `txt_keterangan_pelunasan` text,
  `int_petugas_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `t_pelunasan`
--

INSERT INTO `t_pelunasan` (`int_pelunasan_id`, `int_permohonan_id`, `int_pemeriksaan_id`, `int_status_pelunasan`, `dt_pelunasan`, `txt_keterangan_pelunasan`, `int_petugas_id`) VALUES
(7, 1, 2, 3, '2020-06-18 12:00:10.000000', 'keterangan', 1),
(8, 1, 55, 1, '2020-07-04 00:00:00.000000', 'Test keterangan from app', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_pelunasan_img`
--

CREATE TABLE `t_pelunasan_img` (
  `int_pelunasan_img_id` int(5) NOT NULL,
  `int_pelunasan_id` int(5) DEFAULT NULL,
  `txt_direktori` text,
  `txt_keterangan` text,
  `txt_type` text,
  `txt_size` text,
  `dt_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `t_pelunasan_img`
--

INSERT INTO `t_pelunasan_img` (`int_pelunasan_img_id`, `int_pelunasan_id`, `txt_direktori`, `txt_keterangan`, `txt_type`, `txt_size`, `dt_created`) VALUES
(3, 8, '1_pembayaran_1_2020-07-04_13-21-05.jpg', NULL, 'image', '53502.00', '2020-07-04 00:00:00'),
(4, 8, '1_pembayaran_2_2020-07-04_13-21-05.jpg', NULL, 'image', '351324.00', '2020-07-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_pemeriksaan`
--

CREATE TABLE `t_pemeriksaan` (
  `int_pemeriksaan_id` int(11) NOT NULL,
  `int_permohonan_id` int(11) DEFAULT NULL,
  `txt_fungsi` text,
  `txt_lokasi` text,
  `num_panjang_pemeriksaan` double(15,5) DEFAULT NULL,
  `num_lebar_pemeriksaan` double(15,5) DEFAULT NULL,
  `num_luas_pemeriksaan` double(15,5) DEFAULT NULL,
  `txt_latitude` text,
  `txt_longitude` text,
  `int_jaringan_id` int(11) DEFAULT NULL,
  `int_tarif_fungsi_id` int(11) DEFAULT NULL,
  `txt_no_skr` varchar(20) DEFAULT NULL,
  `num_retribusi` double(15,5) DEFAULT NULL,
  `dt_jatuh_tempo` date DEFAULT NULL,
  `txt_keterangan_pemeriksaan` text,
  `int_penghitung_id` int(11) DEFAULT NULL,
  `int_userid_petugas` int(11) DEFAULT NULL,
  `dt_pemeriksaan_petugas` datetime(6) DEFAULT NULL,
  `int_valid` int(1) DEFAULT NULL COMMENT '0 : belum diperiksa\n1 : belum valid\n2 : valid\n3 : periksa ulang'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `t_pemeriksaan`
--

INSERT INTO `t_pemeriksaan` (`int_pemeriksaan_id`, `int_permohonan_id`, `txt_fungsi`, `txt_lokasi`, `num_panjang_pemeriksaan`, `num_lebar_pemeriksaan`, `num_luas_pemeriksaan`, `txt_latitude`, `txt_longitude`, `int_jaringan_id`, `int_tarif_fungsi_id`, `txt_no_skr`, `num_retribusi`, `dt_jatuh_tempo`, `txt_keterangan_pemeriksaan`, `int_penghitung_id`, `int_userid_petugas`, `dt_pemeriksaan_petugas`, `int_valid`) VALUES
(1, 1, NULL, 'Letak Tanah From App', 54.00000, 23.00000, 1242.00000, '0.0', '0.0', NULL, 1, 'test', 6.98000, '2020-06-18', NULL, 1, 1, '2020-07-04 00:00:00.000000', 1),
(55, 1, 'test fungsi', 'lokasi', 20.09000, 21.98000, 54.99000, 'rerere', 'trtrtr', 3, 1, 'test', 6.98000, '2020-06-18', 'test keterangan', 1, 1, '2020-06-10 14:05:43.000000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_pemeriksaan_img`
--

CREATE TABLE `t_pemeriksaan_img` (
  `int_pemeriksaan_img_id` int(5) NOT NULL,
  `int_pemeriksaan_id` int(5) DEFAULT NULL,
  `txt_direktori` text,
  `txt_keterangan` text,
  `txt_type` text,
  `txt_size` text,
  `dt_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `t_pemeriksaan_img`
--

INSERT INTO `t_pemeriksaan_img` (`int_pemeriksaan_img_id`, `int_pemeriksaan_id`, `txt_direktori`, `txt_keterangan`, `txt_type`, `txt_size`, `dt_created`) VALUES
(39, 1, '1_pemeriksaan_1_2020-07-04_13-15-21.jpg', NULL, 'image', '53502.00', '2020-07-04 00:00:00'),
(40, 1, '1_pemeriksaan_2_2020-07-04_13-15-21.jpg', NULL, 'image', '351324.00', '2020-07-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_permohonan`
--

CREATE TABLE `t_permohonan` (
  `int_permohonan_id` int(11) NOT NULL,
  `var_kode_permohonan` varchar(20) DEFAULT NULL,
  `var_nik_pemohon` varchar(16) DEFAULT NULL,
  `txt_nama_pemohon` text,
  `txt_pekerjaan_pemohon` text,
  `var_telepon_pemohon` varchar(13) DEFAULT NULL,
  `txt_alamat_pemohon` text,
  `txt_koordinat_pemohon` text,
  `dt_permohonan` datetime DEFAULT NULL,
  `int_kecamatan_id_pemohon` int(11) DEFAULT NULL,
  `int_desa_id_pemohon` bigint(10) DEFAULT NULL,
  `txt_tanah_permohonan` text,
  `txt_letak_tanah_permohonan` text,
  `int_kecamatan_id_permohonan` int(7) DEFAULT NULL,
  `int_desa_id_permohonan` bigint(10) DEFAULT NULL,
  `num_panjang_permohonan` double(15,5) DEFAULT NULL,
  `num_lebar_permohonan` double(15,5) DEFAULT NULL,
  `num_luas_permohonan` double(15,5) DEFAULT NULL,
  `txt_batas_utara_permohonan` text,
  `txt_batas_barat_permohonan` text,
  `txt_batas_selatan_permohonan` text,
  `txt_batas_timur_permohonan` text,
  `txt_keperluan_permohonan` text,
  `int_lama_pemakaian_permohonan` int(3) DEFAULT NULL,
  `txt_masa_permohonan` varchar(5) DEFAULT NULL,
  `dt_tgl_awal` date DEFAULT NULL,
  `dt_tgl_akhir` date DEFAULT NULL,
  `int_progress` int(1) DEFAULT NULL COMMENT '0 : permohonan\n\r\n1 : pemeriksaan\n\r\n2 : skr -> terbit surat keterangan retribusi\r\n\n3 : lunas\r\n4 : Deleted',
  `dt_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `dt_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `t_permohonan`
--

INSERT INTO `t_permohonan` (`int_permohonan_id`, `var_kode_permohonan`, `var_nik_pemohon`, `txt_nama_pemohon`, `txt_pekerjaan_pemohon`, `var_telepon_pemohon`, `txt_alamat_pemohon`, `txt_koordinat_pemohon`, `dt_permohonan`, `int_kecamatan_id_pemohon`, `int_desa_id_pemohon`, `txt_tanah_permohonan`, `txt_letak_tanah_permohonan`, `int_kecamatan_id_permohonan`, `int_desa_id_permohonan`, `num_panjang_permohonan`, `num_lebar_permohonan`, `num_luas_permohonan`, `txt_batas_utara_permohonan`, `txt_batas_barat_permohonan`, `txt_batas_selatan_permohonan`, `txt_batas_timur_permohonan`, `txt_keperluan_permohonan`, `int_lama_pemakaian_permohonan`, `txt_masa_permohonan`, `dt_tgl_awal`, `dt_tgl_akhir`, `int_progress`, `dt_created`, `dt_update`) VALUES
(1, NULL, '1234567890123456', 'nama pemohon 1', 'pekerjaan 1', '06756675474', 'Sawojajar', 'koordinat', '2020-06-10 14:13:44', 3508050, 3508050005, 'Tanah Pemohon', 'Letak Tanah', 3508020, 3508020004, 85.93000, 12.34000, 198.34000, 'Batas Utara', 'Batas Barat', 'Batas Selatan', 'Batas Timur', 'Keperluan', 7, '3', '2020-06-11', '2020-06-18', 3, '2020-06-10 14:13:44', '2020-07-04 20:21:06'),
(2, NULL, '1234567890123456', 'nama pemohon 2', 'pekerjaan 2', '06756675474', 'Sawojajar', 'koordinat', '2020-06-10 14:13:44', 3508050, 3508050005, 'Tanah Pemohon', 'Letak Tanah', 3508020, 3508020004, 85.93000, 12.34000, 198.34000, 'Batas Utara', 'Batas Barat', 'Batas Selatan', 'Batas Timur', 'Keperluan', 7, '3', '2020-06-11', '2020-06-18', 1, '2020-06-10 14:13:44', '2020-06-16 10:54:59');

-- --------------------------------------------------------

--
-- Table structure for table `t_permohonan_img`
--

CREATE TABLE `t_permohonan_img` (
  `int_permohonan_img_id` int(5) NOT NULL,
  `int_permohonan_id` int(5) DEFAULT NULL,
  `txt_direktori` text COLLATE latin1_general_ci,
  `var_jenis_img` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `txt_keterangan` text COLLATE latin1_general_ci,
  `txt_type` text COLLATE latin1_general_ci,
  `txt_size` text COLLATE latin1_general_ci,
  `dt_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `dt_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `t_setoran_upt`
--

CREATE TABLE `t_setoran_upt` (
  `int_setoran_upt_id` int(11) NOT NULL,
  `int_upt_id` int(11) DEFAULT NULL,
  `cur_setoran` double(15,5) DEFAULT NULL,
  `dt_setoran` datetime(6) DEFAULT NULL,
  `int_petugas_id` int(11) DEFAULT NULL,
  `dt_created` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_desa`
--
ALTER TABLE `m_desa`
  ADD PRIMARY KEY (`int_desa_id`) USING BTREE,
  ADD KEY `int_kecamatan_id` (`int_kecamatan_id`) USING BTREE;

--
-- Indexes for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
  ADD PRIMARY KEY (`int_jabatan_id`) USING BTREE;

--
-- Indexes for table `m_jaringan`
--
ALTER TABLE `m_jaringan`
  ADD PRIMARY KEY (`int_jaringan_id`) USING BTREE,
  ADD KEY `int_jaringan_id` (`int_jaringan_id`) USING BTREE;

--
-- Indexes for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  ADD PRIMARY KEY (`int_kecamatan_id`) USING BTREE,
  ADD KEY `int_kecamatan_id` (`int_kecamatan_id`) USING BTREE;

--
-- Indexes for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
  ADD PRIMARY KEY (`int_pegawai_id`) USING BTREE;

--
-- Indexes for table `m_tarif_fungsi`
--
ALTER TABLE `m_tarif_fungsi`
  ADD PRIMARY KEY (`int_tarif_fungsi_id`) USING BTREE,
  ADD KEY `int_tarif_fungsi_id` (`int_tarif_fungsi_id`) USING BTREE;

--
-- Indexes for table `m_upt`
--
ALTER TABLE `m_upt`
  ADD PRIMARY KEY (`int_upt_id`) USING BTREE,
  ADD KEY `intIDUPT` (`int_upt_id`) USING BTREE;

--
-- Indexes for table `m_upt_akses_kecamatan`
--
ALTER TABLE `m_upt_akses_kecamatan`
  ADD PRIMARY KEY (`int_upt_akses_kecamatan_id`) USING BTREE,
  ADD UNIQUE KEY `UK_m_user_admin_akses_upt` (`int_upt_id`,`int_kecamatan_id`) USING BTREE,
  ADD KEY `intIDUserAksesAdminUPT` (`int_upt_akses_kecamatan_id`) USING BTREE;

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`intIDuser`) USING BTREE,
  ADD KEY `intIDuser` (`intIDuser`) USING BTREE;
ALTER TABLE `m_user` ADD FULLTEXT KEY `IDX_m_user_txtUsername` (`txtUsername`);

--
-- Indexes for table `m_userpetugas`
--
ALTER TABLE `m_userpetugas`
  ADD PRIMARY KEY (`int_userid_petugas`) USING BTREE,
  ADD KEY `int_userid_petugas` (`int_userid_petugas`) USING BTREE;

--
-- Indexes for table `m_userpetugas_akses`
--
ALTER TABLE `m_userpetugas_akses`
  ADD PRIMARY KEY (`int_userid_petugas_akses`) USING BTREE,
  ADD KEY `int_userid_petugas_akses` (`int_userid_petugas_akses`) USING BTREE;

--
-- Indexes for table `m_user_admin`
--
ALTER TABLE `m_user_admin`
  ADD PRIMARY KEY (`intIDuser`) USING BTREE,
  ADD KEY `intIDuser` (`intIDuser`) USING BTREE,
  ADD KEY `IDX_m_user_admin_intIDGroup` (`intIDGroup`) USING BTREE;
ALTER TABLE `m_user_admin` ADD FULLTEXT KEY `IDX_m_user_admin_txtUsername` (`txtUsername`);

--
-- Indexes for table `m_user_admin_akses_upt`
--
ALTER TABLE `m_user_admin_akses_upt`
  ADD PRIMARY KEY (`intIDUserAksesAdminUPT`) USING BTREE,
  ADD UNIQUE KEY `UK_m_user_admin_akses_upt` (`intIDUser`,`intIDUPT`) USING BTREE,
  ADD KEY `intIDUserAksesAdminUPT` (`intIDUserAksesAdminUPT`) USING BTREE;

--
-- Indexes for table `m_user_admin_group`
--
ALTER TABLE `m_user_admin_group`
  ADD PRIMARY KEY (`intIDGroup`) USING BTREE;

--
-- Indexes for table `m_user_admin_group_menu`
--
ALTER TABLE `m_user_admin_group_menu`
  ADD PRIMARY KEY (`intID`) USING BTREE,
  ADD KEY `IDX_m_user_admin_group_menu_intIDGroup` (`intIDGroup`) USING BTREE,
  ADD KEY `IDX_m_user_admin_group_menu_intIDMenu` (`intIDMenu`) USING BTREE;

--
-- Indexes for table `m_user_admin_menu`
--
ALTER TABLE `m_user_admin_menu`
  ADD PRIMARY KEY (`intIdMenu`) USING BTREE,
  ADD KEY `IDX_m_user_admin_menu_intIdMenu` (`intIdMenu`) USING BTREE,
  ADD KEY `IDX_m_user_admin_menu_intParentId` (`intParentId`) USING BTREE;

--
-- Indexes for table `t_pelunasan`
--
ALTER TABLE `t_pelunasan`
  ADD PRIMARY KEY (`int_pelunasan_id`) USING BTREE,
  ADD UNIQUE KEY `int_pelunasan_id` (`int_pelunasan_id`) USING BTREE,
  ADD KEY `int_permohonan_id` (`int_permohonan_id`) USING BTREE;

--
-- Indexes for table `t_pelunasan_img`
--
ALTER TABLE `t_pelunasan_img`
  ADD PRIMARY KEY (`int_pelunasan_img_id`) USING BTREE,
  ADD UNIQUE KEY `int_pelunasan_img_id` (`int_pelunasan_img_id`) USING BTREE,
  ADD KEY `int_pelunasan_id` (`int_pelunasan_id`) USING BTREE;

--
-- Indexes for table `t_pemeriksaan`
--
ALTER TABLE `t_pemeriksaan`
  ADD PRIMARY KEY (`int_pemeriksaan_id`) USING BTREE,
  ADD UNIQUE KEY `int_pemeriksaan_id` (`int_pemeriksaan_id`) USING BTREE,
  ADD KEY `int_permohonan_id` (`int_permohonan_id`) USING BTREE,
  ADD KEY `dt_pemeriksaan_petugas` (`dt_pemeriksaan_petugas`) USING BTREE;

--
-- Indexes for table `t_pemeriksaan_img`
--
ALTER TABLE `t_pemeriksaan_img`
  ADD PRIMARY KEY (`int_pemeriksaan_img_id`) USING BTREE,
  ADD UNIQUE KEY `int_pemeriksaan_img_id` (`int_pemeriksaan_img_id`) USING BTREE,
  ADD KEY `int_pemeriksaan_id` (`int_pemeriksaan_id`) USING BTREE;

--
-- Indexes for table `t_permohonan`
--
ALTER TABLE `t_permohonan`
  ADD PRIMARY KEY (`int_permohonan_id`) USING BTREE,
  ADD UNIQUE KEY `int_permohonan_id` (`int_permohonan_id`) USING BTREE,
  ADD KEY `int_kecamatan_id_pemohon` (`int_kecamatan_id_pemohon`) USING BTREE,
  ADD KEY `dt_permohonan` (`dt_permohonan`) USING BTREE;

--
-- Indexes for table `t_permohonan_img`
--
ALTER TABLE `t_permohonan_img`
  ADD PRIMARY KEY (`int_permohonan_img_id`) USING BTREE,
  ADD UNIQUE KEY `int_permohonan_img_id` (`int_permohonan_img_id`) USING BTREE,
  ADD KEY `int_permohonan_id` (`int_permohonan_id`) USING BTREE;

--
-- Indexes for table `t_setoran_upt`
--
ALTER TABLE `t_setoran_upt`
  ADD PRIMARY KEY (`int_setoran_upt_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_desa`
--
ALTER TABLE `m_desa`
  MODIFY `int_desa_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3508200012;

--
-- AUTO_INCREMENT for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
  MODIFY `int_jabatan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_jaringan`
--
ALTER TABLE `m_jaringan`
  MODIFY `int_jaringan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  MODIFY `int_kecamatan_id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3508201;

--
-- AUTO_INCREMENT for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
  MODIFY `int_pegawai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `m_tarif_fungsi`
--
ALTER TABLE `m_tarif_fungsi`
  MODIFY `int_tarif_fungsi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_upt`
--
ALTER TABLE `m_upt`
  MODIFY `int_upt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_upt_akses_kecamatan`
--
ALTER TABLE `m_upt_akses_kecamatan`
  MODIFY `int_upt_akses_kecamatan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `intIDuser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_userpetugas`
--
ALTER TABLE `m_userpetugas`
  MODIFY `int_userid_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_userpetugas_akses`
--
ALTER TABLE `m_userpetugas_akses`
  MODIFY `int_userid_petugas_akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_user_admin`
--
ALTER TABLE `m_user_admin`
  MODIFY `intIDuser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_user_admin_akses_upt`
--
ALTER TABLE `m_user_admin_akses_upt`
  MODIFY `intIDUserAksesAdminUPT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_user_admin_group`
--
ALTER TABLE `m_user_admin_group`
  MODIFY `intIDGroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_user_admin_group_menu`
--
ALTER TABLE `m_user_admin_group_menu`
  MODIFY `intID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `m_user_admin_menu`
--
ALTER TABLE `m_user_admin_menu`
  MODIFY `intIdMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `t_pelunasan`
--
ALTER TABLE `t_pelunasan`
  MODIFY `int_pelunasan_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_pelunasan_img`
--
ALTER TABLE `t_pelunasan_img`
  MODIFY `int_pelunasan_img_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_pemeriksaan`
--
ALTER TABLE `t_pemeriksaan`
  MODIFY `int_pemeriksaan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `t_pemeriksaan_img`
--
ALTER TABLE `t_pemeriksaan_img`
  MODIFY `int_pemeriksaan_img_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `t_permohonan`
--
ALTER TABLE `t_permohonan`
  MODIFY `int_permohonan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `t_permohonan_img`
--
ALTER TABLE `t_permohonan_img`
  MODIFY `int_permohonan_img_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `t_setoran_upt`
--
ALTER TABLE `t_setoran_upt`
  MODIFY `int_setoran_upt_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
