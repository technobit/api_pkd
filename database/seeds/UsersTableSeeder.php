<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $method = 'AES-128-CBC';
        $key = 'key_incar';
        $iv = 'Incar_Password20';

        DB::table('m_userpetugas')->insert([
            'txt_namadepan'         => 'Super',
            'txt_namabelakang'      => 'User',
            'txt_alamat'            => 'Jl. Danau Sentani Dalam H1L4',
            'txt_email'             => 'superuser@gmail.com',
            'txt_username'          => 'superuser',
            // 'txt_password'          => openssl_encrypt('superuser', $method, $key, 0, $iv),
            'txt_password'          => 'superuser',
            'txt_avatar'            => 'default.png',
            'is_aktif'              => 1,
            'dt_created'            => now(),
        ]);
    }
}
