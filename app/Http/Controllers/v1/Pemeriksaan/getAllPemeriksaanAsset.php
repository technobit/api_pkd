<?php

namespace App\Http\Controllers\v1\Pemeriksaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pemeriksaan;

class getAllPemeriksaanAsset extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $data = DB::select('call mobile_get_allPemeriksaan_asset(?,?,?,?)', [
            $request[Pemeriksaan::ID_PETUGAS] ?? null,
            -1,
            -1,
            -1
        ]);

        return APIresponse(true, 'Data Pemeriksaan Asset Berhasil Ditemukan!', $data);
    }
}
