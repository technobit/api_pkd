<?php

namespace App\Http\Controllers\v1\Pemeriksaan;

use App\Http\Controllers\Controller;
use App\Models\Pemeriksaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class getPemeriksaanAsset extends Controller {
    
    function __invoke(Request $request) {

        $request = $request->toArray();

        $data = DB::select('call mobile_get_pemeriksaan_asset(?,?,?,?,?,?)', [
            $request[Pemeriksaan::ID_PETUGAS] ?? null ,
            $request[Pemeriksaan::PROGRESS] ?? null ,
            $request[Pemeriksaan::ID_KECAMATAN] ?? null ,
            $request[Pemeriksaan::ID_DESA] ?? null ,
            $request[Pemeriksaan::DT_AWAL] ?? null ,
            $request[Pemeriksaan::DT_AKHIR] ?? null 
        ]);

        return APIresponse(true, 'Data Pemeriksaan Asset Berhasil Ditemukan!', $data);
    }
}
