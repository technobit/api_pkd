<?php

namespace App\Http\Controllers\v1\Pemeriksaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Pemeriksaan;

class getDataPemohonPemeriksaan extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Pemeriksaan::IN_ID_PEMOHON      => 'required',
            Pemeriksaan::ID_PEMERIKSAAN     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_get_datapemohon_pemeriksaan(?,?)', [
            $request[Pemeriksaan::IN_ID_PEMOHON],
            $request[Pemeriksaan::ID_PEMERIKSAAN]
        ]);

        return APIresponse(true, 'Data Pemohon Pemeriksaan Berhasil Ditemukan!', $data);
    }
}
