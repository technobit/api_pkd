<?php

namespace App\Http\Controllers\v1\Pemeriksaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pemeriksaan;
use Illuminate\Support\Carbon;

class insertDataPemeriksaan extends Controller {
    
    const imagePath = 'images/pemeriksaan';

    function __invoke(Request $request) {
        $now = Carbon::now();
        $credentials = [];

        if ($request->hasfile(Pemeriksaan::F_FOTO)) { 
            $index = 1;
            foreach ($request->file(Pemeriksaan::F_FOTO) as $image) {
                // $files = $value;
                // $index = $key+1;
                $destinationPath = public_path(SELF::imagePath);
                $filename = $request->in_int_permohonan_id.'_pemeriksaan_'.$index.'_'.$now->format('Y-m-d_H-i-s').'.'.$image->getClientOriginalExtension();
                $credentials[$index][Pemeriksaan::FILE_SIZE] = $image->getSize();
                $image->move($destinationPath, $filename);
                $credentials[$index][Pemeriksaan::FILE_NAME] = $filename;
                $index++;
            }
        }

        $request = $request->toArray();

        DB::select('call mobile_ins_datapemeriksaan(?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request[Pemeriksaan::ID_PEMERIKSAAN],
            $request[Pemeriksaan::ID_PERMOHONAN] ?? null ,
            $request[Pemeriksaan::FUNGSI] ?? null ,
            $request[Pemeriksaan::LOKASI] ?? null ,
            $request[Pemeriksaan::PNJG_PEMERIKSAAN] ?? null ,
            $request[Pemeriksaan::LBAR_PEMERIKSAAN] ?? null ,
            $request[Pemeriksaan::LUAS_PEMERIKSAAN] ?? null ,
            $request[Pemeriksaan::LATITUDE] ?? null ,
            $request[Pemeriksaan::LONGITUDE] ?? null ,
            $request[Pemeriksaan::ID_JARINGAN] ?? null ,
            $request[Pemeriksaan::KET_PEMERIKSAAN] ?? null ,
            $request[Pemeriksaan::ID_PETUGAS] ?? null ,
            $request[Pemeriksaan::DT_PEMERIKSAAN] ?? null 
        ]);

        foreach ($credentials as $key => $value) {
            // $request[$key] = $value;
            DB::select('call mobile_ins_fotopemeriksaan(?,?,?,?)', [
                $request[Pemeriksaan::ID_PEMERIKSAAN],
                // $dataPemeriksaan[0]->int_pemeriksaan_id,
                $value[Pemeriksaan::FILE_NAME] ,
                $value[Pemeriksaan::FILE_SIZE] ,
                $request[Pemeriksaan::DT_UPLOAD] ?? null 
            ]);
        }

        return APIresponse(true, 'Data Pemeriksaan Berhasil Disimpan!', null);
    }
}
