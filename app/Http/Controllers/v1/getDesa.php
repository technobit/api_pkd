<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\UserPetugas;

class getDesa extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            UserPetugas::IN_ID_KEC  => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_get_desa(?)', [
            $request[UserPetugas::IN_ID_KEC]
        ]);

        return APIresponse(true, 'Data Desa Berhasil Ditemukan!', $data);
    }
}
