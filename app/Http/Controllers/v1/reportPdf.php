<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class reportPdf extends Controller {

    const imagePath = 'images/pdf';

    function __invoke(Request $request) {
        $data = [
            'nama_usaha'        => $request->nama_usaha,
            'nama_pemilik'      => $request->nama_pemilik,
            'nama_proyek'       => $request->nama_proyek,
            'jenis'             => $request->jenis,
            'nama_audit'        => $request->nama_audit,
            'tanggal_laporan'   => $request->tanggal_laporan,
            'kuisioner'         => $request->kuisioner,
            'foto'              => $request->foto,
        ];

        $now = Carbon::now();
        // $credentials = [];

        if ($request->hasfile('file_foto')) { 
            $index = 0;
            foreach ($request->file('file_foto') as $key => $image) {
                $destinationPath = public_path(SELF::imagePath);
                $filename = 'file_report_'.$key.'_'.$now->format('Y-m-d_H-i-s').'.'.$image->getClientOriginalExtension();
                $data['file_foto'][$index]['index'] = $key;
                $image->move($destinationPath, $filename);
                $data['file_foto'][$index]['filename'] = $filename;
                $index++;
            }
        }

        // dd($data);
        // return response([
        //     'data' => $data
        // ]);
        return view('printPdf', compact('data'));
    }
}
