<?php

namespace App\Http\Controllers\v1\Pembayaran;

use App\Http\Controllers\Controller;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class getDataPemohonPembayaran extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $data = DB::select('call mobile_get_datapemohon_pembayaran(?,?)', [
            $request[Pembayaran::ID_PERMOHONAN],
            $request[Pembayaran::ID_PEMERIKSAAN]
        ]);

        return APIresponse(true, 'Data Pemohon Pembayaran Berhasil Ditemukan!', $data);
    }
}
