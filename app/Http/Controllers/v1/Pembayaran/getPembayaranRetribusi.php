<?php

namespace App\Http\Controllers\v1\Pembayaran;

use App\Http\Controllers\Controller;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class getPembayaranRetribusi extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $data = DB::select('call mobile_get_pembayaran_retribusi(?,?,?,?,?,?)', [
            $request[Pembayaran::ID_PETUGAS] ?? null ,
            $request[Pembayaran::PROGRESS] ?? null ,
            $request[Pembayaran::ID_KECAMATAN] ?? null ,
            $request[Pembayaran::ID_DESA] ?? null ,
            $request[Pembayaran::DT_AWAL] ?? null ,
            $request[Pembayaran::DT_AKHIR] ?? null 
        ]);

        return APIresponse(true, 'Data Pembayaran Retribusi Berhasil Ditemukan!', $data);
    }
}
