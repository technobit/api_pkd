<?php

namespace App\Http\Controllers\v1\Pembayaran;

use App\Http\Controllers\Controller;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class insertDataPembayaran extends Controller {

    const imagePath = 'images/pembayaran';

    function __invoke(Request $request) {
        $now = Carbon::now();
        $credentials = [];

        if ($request->hasfile(Pembayaran::F_FOTO)) { 
            $index = 1;
            foreach ($request->file(Pembayaran::F_FOTO) as $image) {
                // $files = $value;
                // $index = $key+1;
                $destinationPath = public_path(SELF::imagePath);
                $filename = $request->in_int_permohonan_id.'_pembayaran_'.$index.'_'.$now->format('Y-m-d_H-i-s').'.'.$image->getClientOriginalExtension();
                $credentials[$index][Pembayaran::FILE_SIZE] = $image->getSize();
                $image->move($destinationPath, $filename);
                $credentials[$index][Pembayaran::FILE_NAME] = $filename;
                $index++;
            }
        }

        $request = $request->toArray();

        $dataPembayaran = DB::select('call mobile_ins_datapembayaran(?,?,?,?,?)', [
            $request[Pembayaran::ID_PEMERIKSAAN] ?? null,
            $request[Pembayaran::ID_PERMOHONAN] ?? null,
            $request[Pembayaran::KETERANGAN] ?? null,
            $request[Pembayaran::ID_PETUGAS] ?? null,
            $request[Pembayaran::DT_PEMERIKSAAN] ?? null
        ]);

        $idPembayaran = $dataPembayaran[0]->int_pelunasan_id;

        foreach ($credentials as $key => $value) {
            // $request[$key] = $value;
            DB::select('call mobile_ins_fotopembayaran(?,?,?,?)', [
                $idPembayaran,
                // $dataPembayaran[0]->int_Pembayaran_id,
                $value[Pembayaran::FILE_NAME] ,
                $value[Pembayaran::FILE_SIZE] ,
                $request[Pembayaran::DT_UPLOAD] ?? null 
            ]);
        }

        // $filePembayaran = DB::select('call mobile_ins_fotopembayaran(?,?,?,?)', [
        //     $dataPembayaran[0]->int_pelunasan_id,
        //     $request[Pembayaran::FILE_NAME] ?? null,
        //     $request[Pembayaran::FILE_SIZE] ?? null,
        //     $request[Pembayaran::DT_UPLOAD] ?? null
        // ]);

        return APIresponse(true, 'Data Permbayaran Berhasil Disimpan!', null);
    }
}
