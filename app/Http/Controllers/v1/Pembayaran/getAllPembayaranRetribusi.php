<?php

namespace App\Http\Controllers\v1\Pembayaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pembayaran;

class getAllPembayaranRetribusi extends Controller {
    
    function __invoke(Request $request) {
        $request = $request->toArray();

        $data = DB::select('call mobile_get_allPembayaran_retribusi(?)', [
            $request[Pembayaran::ID_PETUGAS] ?? null,
            -1,
            -1,
            -1
        ]);

        return APIresponse(true, 'Data Pembayaran Retribusi Berhasil Ditemukan!', $data);
    }
}
