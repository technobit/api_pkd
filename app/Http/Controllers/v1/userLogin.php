<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class userLogin extends Controller {
    
    const USERNAME = 'in_txtusername';
    const PASSWORD = 'in_txtpassword';

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SELF::USERNAME      => 'required',
            SELF::PASSWORD      => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_get_userAccount(?)', [
            $request[SELF::USERNAME]
            // $request[SELF::PASSWORD]
        ]);

        if (!$data[0]->success || !verifyPassword($request[SELF::PASSWORD], $data[0]->txt_password)) 
            return APIresponse(false, 'Username Atau Password Tidak Ditemukan!', null);

        unset($data[0]->txt_password);
        return APIresponse(true, 'Berhasil Login!', $data[0]);
    }
}
