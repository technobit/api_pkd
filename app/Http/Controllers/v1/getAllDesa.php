<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getAllDesa extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_get_allDesa');

        return APIresponse(true, 'Data Desa Berhasil Ditemukan!', $data);
    }
}
