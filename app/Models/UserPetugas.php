<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPetugas extends Model {
    
    // const ID = 'int_userid_petugas';
    // const NAMA_DEPAN = 'txt_namadepan';
    // const NAMA_BLKG = 'txt_namabelakang';
    // const ALAMAT = 'txt_alamat';
    // const EMAIL = 'txt_email';
    // const USERNAME = 'txt_username';
    // const PASSWORD = 'txt_password';
    // const AVATAR = 'txt_avatar';
    // const IS_ACTIVE = 'is_aktif';
    // const CREATED = 'dt_created';

    const IN_ID = 'in_int_userid_petugas';
    const IN_ID_KEC = 'in_int_kecamatan_id';
}
