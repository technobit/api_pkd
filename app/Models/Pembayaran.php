<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model {
    
    const ID_PETUGAS = 'in_int_userid_petugas';
    const ID_PERMOHONAN = 'in_int_permohonan_id';
    const ID_KECAMATAN = 'in_int_kecamatan_id';
    const ID_DESA = 'in_int_desa_id';
    const ID_PEMERIKSAAN = 'in_int_pemeriksaan_id';
    const PROGRESS = 'in_int_progress';
    const KETERANGAN = 'in_txt_keterangan';
    const DT_PEMERIKSAAN = 'in_dt_pemeriksaan_petugas';
    const FILE_NAME = 'in_txt_filename';
    const FILE_SIZE = 'in_txt_size';
    const DT_UPLOAD = 'in_dtupload';
    const DT_AWAL = 'in_dtawal';
    const DT_AKHIR = 'in_dtakhir';

    const F_FOTO = 'in_file_photo';
}
