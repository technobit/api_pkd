<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemeriksaan extends Model {
    
    const IN_ID_PEMOHON = 'in_int_permohonan_id';
    const ID_JARINGAN = 'in_int_jaringan_id';
    const ID_PETUGAS = 'in_int_userid_petugas';
    const ID_PERMOHONAN = 'in_int_permohonan_id';
    const ID_PEMERIKSAAN = 'in_int_pemeriksaan_id';
    const ID_KECAMATAN = 'in_int_kecamatan_id';
    const ID_DESA = 'in_int_desa_id';
    const FUNGSI = 'in_txt_fungsi';
    const PROGRESS = 'in_int_progress';
    const PNJG_PEMERIKSAAN = 'in_num_panjang_pemeriksaan';
    const LBAR_PEMERIKSAAN = 'in_num_lebar_pemeriksaan';
    const LUAS_PEMERIKSAAN = 'in_num_luas_pemeriksaan';
    const LOKASI = 'in_txt_lokasi';
    const LATITUDE = 'in_txt_latitude';
    const LONGITUDE = 'in_txt_longitude';
    const KET_PEMERIKSAAN = 'in_txt_keterangan_pemeriksaan';
    const DT_PEMERIKSAAN = 'in_dt_pemeriksaan_petugas';
    const FILE_NAME = 'in_txt_filename';
    const FILE_SIZE = 'in_txt_size';
    const DT_UPLOAD = 'in_dtupload';
    const DT_AWAL = 'in_dtawal';
    const DT_AKHIR = 'in_dtakhir';

    const F_FOTO = 'in_file_photo';
}