<?php

if (!function_exists('verifyPassword')) {
    function verifyPassword($password, $hash) {
        if (strlen($hash) !== 60 OR strlen($password = crypt($password, $hash)) !== 60)
			return FALSE;

		$compare = 0;
		for ($i = 0; $i < 60; $i++)
            $compare |= (ord($password[$i]) ^ ord($hash[$i]));

		return ($compare === 0);
    }
}

?>