<?php
    
    if (!function_exists('APIresponse')) {
        function APIresponse($status, $message, $data, $code = 200) {
            return response([
                API_STATUS      => $status,
                API_MESSAGE     => $message,
                API_DATA        => $data
            ], $code);
        }
    }
?>