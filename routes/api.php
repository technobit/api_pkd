<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/', function () {
    return response([
        'app' => config('app.name'),
        'version' => '1.0.0',
    ],200);
});
Route::post('userLogin', 'v1\userLogin');

Route::post('reportPdf', 'v1\reportPdf');

Route::group(array('middleware' => ['isAuthorized']), function (){

    Route::get('getDashboard', 'v1\getDashboard');
    Route::get('getKecamatan', 'v1\getKecamatan');
    Route::get('getDesa', 'v1\getDesa');
    Route::get('getAllDesa', 'v1\getAllDesa');

    Route::prefix('pemeriksaan')->group(function () {
        Route::get('getPemeriksaanAsset', 'v1\Pemeriksaan\getPemeriksaanAsset');
        Route::get('getAllPemeriksaanAsset', 'v1\Pemeriksaan\getAllPemeriksaanAsset');
        Route::get('getDataPemohonPemeriksaan', 'v1\Pemeriksaan\getDataPemohonPemeriksaan');
        Route::get('getJaringanIrigasi', 'v1\Pemeriksaan\getJaringanIrigasi');
        Route::post('insertDataPemeriksaan', 'v1\Pemeriksaan\insertDataPemeriksaan');
    });

    Route::prefix('pembayaran')->group(function() {
        Route::get('getPembayaranRetribusi', 'v1\Pembayaran\getPembayaranRetribusi');
        Route::get('getAllPembayaranRetribusi', 'v1\Pembayaran\getAllPembayaranRetribusi');
        Route::get('getDataPemohonPembayaran', 'v1\Pembayaran\getDataPemohonPembayaran');
        Route::post('insertDataPembayaran', 'v1\Pembayaran\insertDataPembayaran');
    });
});

// Route::group(array('middleware' => ['isAuthorized']), function (){

    // Route::get('getDesa', 'v1\getDesa');
    // Route::get('getKecamatan', 'v1\getKecamatan');
    // Route::get('getDataJaringanIrigasi', 'v1\getDataJaringanIrigasi');
    // Route::get('getJenisSaluran', 'v1\getJenisSaluran');
    // Route::get('getSaluran', 'v1\getSaluran');

// });

