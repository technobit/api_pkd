<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Laporan Pdf</title>

  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/favicon.png') }}">
    <title>Laporan</title>
    <!-- Custom CSS -->
    <link href="{{ asset('/dist/css/style.css') }}" rel="stylesheet">

    <!-- font icon -->
    <link href="{{ asset('/assets/libs/elegant-icons-style.css') }}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{ asset('/assets/libs/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/style-responsive.css') }}" rel="stylesheet" />
    <script type="text/javascript" charset="utf8" src="{{ asset('/assets/libs/jquery/jquery-3.4.1.min.js') }}"></script>
    <script>
      $(document).ready(function(){
        setTimeout(function() {
            window.focus();
            window.print();
            window.close();
        },1000)
      });
    </script>
    <style>
      @media all {
      .page-break { display: none; }
      }

      @media print {
      .page-break { display: block; page-break-before: always; }
      }
    </style>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-img3-body" >
  <div class="container">
    <div class="row mt-5">
      <div class="col-sm-12">
        <h2 style="color:red;font-weight:bold">Sikkat</h2>
        <h2 style="font-weight:bold">Laporan Audit Mandiri</h2>
        
        <div class="row mt-3">
          <div class="col-sm-6">
            <h4>Nama Usaha</h4>
            <h4>{{ $data['nama_usaha'] }}</h4>
          </div>
          <div class="col-sm-6">
            <h4>Nama Pemilik</h4>
            <h4>{{ $data['nama_pemilik'] }}</h4>
          </div>
        </div>

        <div class="row mt-4 pt-3" style="border-top:1px solid #a4a4a5;">
          <div class="col-sm-6">
            <h4>Nama Proyek : {{ $data['nama_proyek'] }}</h4>
            <h4>Jenis : {{ $data['jenis'] }}</h4>
          </div>
          <div class="col-sm-6">
            <h4>Nama Audit : {{ $data['nama_audit'] }}</h4>
            <h4>Tanggal Laporan : {{ $data['tanggal_laporan'] }}</h4>
          </div>
        </div>

        <div class="row mt-4">
          <div class="col-sm-12">
            <table class="table table-sm" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th colspan="5" style="font-weight:bold">Bagian 1 dari 2 : Audit</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['kuisioner'] as $keyKuisioner => $valKuisioner)
                <tr>
                  <td colspan="5">{{ $loop->iteration.'. '.$valKuisioner['pertanyaan'] }}</td>
                </tr>
                <tr style="text-align: center;">
                  <td>Ada</td>
                  <td>Tidak Ada</td>
                  <td>Tidak Sesuai</td>
                  <td>Tanggal Pemebenahan</td>
                  <td>Komentar</td>
                </tr>
                <tr style="text-align: center;">
                  <td>{{ $valKuisioner['jawaban'] == 1 ? 'V' : '-' }}</td>
                  <td>{{ $valKuisioner['jawaban'] == 2 ? 'V' : '-' }}</td>
                  <td>{{ $valKuisioner['jawaban'] == 3 ? 'V' : '-' }}</td>
                  <td>{{ $valKuisioner['tanggal_pembenahan'] }}</td>
                  <td>{{ $valKuisioner['komentar'] }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>

    {{-- Photos  --}}
    @forelse ($data['foto'] as $keyFoto => $valFoto)
      <div class="page-break mt-3">
        <div class="row">
          <div class="col-sm-12">
            <h2 style="font-weight: bold">Foto</h2>
            <div class="row">
              <div class="col-sm-6">
                <h4>Kriteria</h4>
                <h4>{{ $valFoto['kriteria'] }}</h4>
              </div>
              <div class="col-sm-6">
                <h4>Sub Kriteria</h4>
                <h4>{{ $valFoto['sub_kriteria'] }}</h4>
              </div>
            </div>

            <div class="row mt-4 pt-3" style="border-top:1px solid #a4a4a5;">
              <div class="col-sm-12">
                <h4>{{ $loop->iteration.'. '.$valFoto['pertanyaan'] }}</h4>
              </div>
            </div>

            <div class="row mt-4">
              <div class="col-sm-12">
                @forelse ($data['file_foto'] as $keyFile => $valFile)
                  @if ($valFile['index'] == $valFoto['index'])
                    <img src="{{ url('/').'/public/images/pdf/'.$valFile['filename'] }}" alt="" srcset="" style="max-width:100%;max-height:100%">
                  @endif
                @empty
        
                @endforelse
              </div>
            </div>
          </div>
        </div>
      </div>
    @empty
        
    @endforelse
  </div>

</body>

</html>
